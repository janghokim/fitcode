//--------------------
// completion.h
//--------------------
extern char * dupstr (char *s);
extern void initialize_readline (void);
char * command_generator (const char *text, int state);
char ** specfit_completion (const char *text, int start, int end);
