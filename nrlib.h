#ifndef __NRLIB_H__
#define __NRLIB_H__

bool svdcmp(double **a, int m, int n, double w[], double **v);
int newt(double x[], int n, bool &check,
          void (*vecfunc)(int, double [], double []));

#define DSQR(a) ((a) == 0.0 ? 0.0 : (a)*(a))
#define SIGN(a,b) ((b) >= 0.0 ? (double)fabs(a) : -(double)fabs(a))
#define IMIN(a,b) ((a) < (b) ? (a) : (b))
#define DMAX(a,b) ((a) > (b) ? (a) : (b))

#define vector(ptr,sz,type)                                                \
        do {                                                               \
          ptr = (type *)malloc(sz * sizeof (type));                        \
          if (!ptr) {                                                      \
            fprintf(stderr, "%s:%d: " #ptr " failed to be allocated.\n",   \
                    __FILE__, __LINE__);                                   \
            exit(1);                                                       \
          }                                                                \
        } while (0)

#define matrix(ptr,row,col,type)                                           \
        do {                                                               \
          ptr = (type **)malloc(row * sizeof (type *));                    \
          if (!ptr) {                                                      \
            fprintf(stderr, "%s:%d: " #ptr " failed to be allocated.\n",   \
                    __FILE__, __LINE__);                                   \
          }                                                                \
          for (int ii = 0; ii < col; ii++) {                               \
            ptr[ii] = (type *)malloc(col * sizeof (type));                 \
            if (!ptr[ii]) {                                                \
              fprintf(stderr, "%s:%d: " #ptr                               \
                              "[%d] failed to be allocated.\n",            \
                      __FILE__, __LINE__, ii);                             \
              for (int jj = ii-1; jj >= 0; jj--) {                         \
                free(ptr[jj]);                                             \
              }                                                            \
              free(ptr);                                                   \
            }                                                              \
          }                                                                \
        } while (0)

#define free_matrix(ptr,row)                                               \
        do {                                                               \
          for (int jj = 0; jj < row; jj++) {                               \
            free(ptr[jj]);                                                 \
          }                                                                \
          free(ptr);                                                       \
        } while (0)

#endif
