//--------------------
//
//   stripwhite.h
//
//--------------------
#include "global.h"
#include <string>
#include <vector>

using namespace std;

//-------------------------------------------------------
//  Strip whitespace from the start and end of STRING.  
//  Return a pointer into STRING. 
//-------------------------------------------------------
extern char * trim (char *string);

//------------------------------------------------------
// make array of commands from a line of multi-commands
//------------------------------------------------------
extern void hackLine(char *s, char256 *comm, char c, int &num);

//---------------------
// tokenize C++ string
//---------------------
extern void tokenize(const string& str, 
                     vector<string>& tokens,
                     const string& delimiters = " ");
