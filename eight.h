//------------------------------------------------------
//  eight.h
//
//  Dec 3, 2007. created by rvanguard
//------------------------------------------------------
#if ( defined FIT_SWME_m1m2 )

extern double a0_l[MAXNT*MAXCFG];
extern double a0_r[MAXNT*MAXCFG];
extern double p_l [MAXNT*MAXCFG];
extern double p_r [MAXNT*MAXCFG];

extern double eight_ss_1c[MAXNT*MAXCFG];
extern double eight_pp_1c[MAXNT*MAXCFG];
extern double eight_vv_1c[MAXNT*MAXCFG];
extern double eight_aa_1c[MAXNT*MAXCFG];

extern double eight_ss_2c[MAXNT*MAXCFG];
extern double eight_pp_2c[MAXNT*MAXCFG];
extern double eight_vv_2c[MAXNT*MAXCFG];
extern double eight_aa_2c[MAXNT*MAXCFG];


extern double sum_a0_l[MAXNT];
extern double sum_a0_r[MAXNT];
extern double sum_p_l [MAXNT];
extern double sum_p_r [MAXNT];

extern double sum_eight_ss_1c[MAXNT];
extern double sum_eight_pp_1c[MAXNT];
extern double sum_eight_vv_1c[MAXNT];
extern double sum_eight_aa_1c[MAXNT];

extern double sum_eight_ss_2c[MAXNT];
extern double sum_eight_pp_2c[MAXNT];
extern double sum_eight_vv_2c[MAXNT];
extern double sum_eight_aa_2c[MAXNT];


// macros for index function.  
#define ind_numx(j,i) ind_ntxcfg("ind_numx",j,i)
#define ind_numx_aa(j,i) ind_ntxcfg("ind_numx_aa",j,i)
#define ind_numx_vv(j,i) ind_ntxcfg("ind_numx_vv",j,i)
#define ind_numx_a1p(j,i) ind_ntxcfg("ind_numx_a1p",j,i)
#define ind_numx_a1n(j,i) ind_ntxcfg("ind_numx_a1n",j,i)
#define ind_numx_a2p(j,i) ind_ntxcfg("ind_numx_a2p",j,i)
#define ind_numx_a2n(j,i) ind_ntxcfg("ind_numx_a2n",j,i)
#define ind_numx_v1p(j,i) ind_ntxcfg("ind_numx_v1p",j,i)
#define ind_numx_v1n(j,i) ind_ntxcfg("ind_numx_v1n",j,i)
#define ind_numx_v2p(j,i) ind_ntxcfg("ind_numx_v2p",j,i)
#define ind_numx_v2n(j,i) ind_ntxcfg("ind_numx_v2n",j,i)

#define ind_a0_l(j,i) ind_ntxcfg("ind_a0_l",j,i)
#define ind_a0_r(j,i) ind_ntxcfg("ind_a0_r",j,i)
#define ind_p_l(j,i) ind_ntxcfg("ind_p_l",j,i)
#define ind_p_r(j,i) ind_ntxcfg("ind_p_r",j,i)
  
#define ind_eight_ss_1c(j,i) ind_ntxcfg("ind_eight_ss_1c",j,i)
#define ind_eight_pp_1c(j,i) ind_ntxcfg("ind_eight_pp_1c",j,i)
#define ind_eight_vv_1c(j,i) ind_ntxcfg("ind_eight_vv_1c",j,i)
#define ind_eight_aa_1c(j,i) ind_ntxcfg("ind_eight_aa_1c",j,i)

#define ind_eight_ss_2c(j,i) ind_ntxcfg("ind_eight_ss_2c",j,i)
#define ind_eight_pp_2c(j,i) ind_ntxcfg("ind_eight_pp_2c",j,i)
#define ind_eight_vv_2c(j,i) ind_ntxcfg("ind_eight_vv_2c",j,i)
#define ind_eight_aa_2c(j,i) ind_ntxcfg("ind_eight_aa_2c",j,i)

extern void zero_eight();
extern void init_eight_cha();
extern void get_eight_dat(int t, int i, int nc);
extern void get_eight_avg(int t, double samp);

#endif 
