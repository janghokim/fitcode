//==========================================================
//
//  FILE: multi1d.h
//
//  Created: 2012/11/07 [integration]
//  Lastest update: 2012/11/13 [integration]
//
//==========================================================

#ifndef __MULTI1D_H__
#define __MULTI1D_H__

#include <iostream>
#include <cstdlib>
using namespace std;

const int MAXDIM = 6;

template< class T >
class multi1d{

  private:
    int ihr_N, ihr_length[MAXDIM], ihr_subvol[MAXDIM], ihr_volume; // inherence variables.
    int N, length[MAXDIM], subvol[MAXDIM], volume, origin;         //
    T *matrix;

  public:
// Constructor
    multi1d( int, int = 0, int = 0, int = 0, int = 0, int = 0 );

// Destructor
    ~multi1d();
    
// Member functions
    void validate( int [] );
    int getVolume( int [], int [] );
    void restore();
    void coalesce( int, int );
    void subarray( int, int );
    int offset( int, int = 0, int = 0, int = 0, int = 0, int = 0 );
    T get( int, int = 0, int = 0, int = 0, int = 0, int = 0 );
//    T get2d( int, int );
    void set( T, int, int = 0, int = 0, int = 0 , int = 0, int = 0 );
//    void set1d( T, int );
    istream& set( istream&, int, int = 0, int = 0, int = 0, int = 0, int = 0 );
    void zerod();
    int dim();
    int vol();
    int len( int );

};


//=============    
// Constructor
//=============    
template< class T >
multi1d< T >::multi1d( int i, int j, int k, int l, int m, int n ){

  int mu;

// Length of matrix index
  ihr_length[0] = i; 
  ihr_length[1] = j; 
  ihr_length[2] = k;
  ihr_length[3] = l;
  ihr_length[4] = m;
  ihr_length[5] = n;

// Calculate dimension
  ihr_N = 0;
  for( mu = 0; mu < MAXDIM; mu++ ){
    if( ihr_length[mu] != 0 ){
      ihr_N += 1;
    }else{
      break;
    }
  }
  N = ihr_N;

  validate( ihr_length );
  
// Calculate volume and subvolumes
  ihr_volume = getVolume( ihr_length, ihr_subvol );

// Assign the inherence values 
// to the matrix properties
  restore();

// Allocate the memory
  matrix =  new T[ihr_volume];
  
//========================TEST=================================
//  cout << N << ", " << origin << endl;
//  for( mu = 0; mu < MAXDIM; mu++ ){
//    cout << "subvol[" << mu << "]=" << subvol[mu] << endl;
//  }
//  cout << "multi1d class object is created." << endl;
//=============================BLOCK===========================
}

//============    
// Destructor
//============    
template< class T >
multi1d< T >::~multi1d(){
  delete [] matrix;
}

//===========================    
// Member function templates
//===========================    
template< class T >
void multi1d< T >::validate( int index[MAXDIM] ){
  
  int mu;

  for( mu = 0; mu < N; mu++ ){
    if( index[mu] < 0 ){
      cerr << "ERROR: validate():: Index should be checked." << endl;
      exit(1001);
    }
  }

  for( mu = N; mu < MAXDIM; mu++ ){
    if( index[mu] != 0 ){
      cerr << "ERROR: validate():: Index should be checked." << endl;
      exit(1002);
    }
  }

}

//==========================================================    
template< class T >
int multi1d< T >::getVolume( int len[MAXDIM], int subv[MAXDIM] ){
  
  int mu, nu, vol;

  vol = 1;
  for( mu = 0; mu < MAXDIM; mu++ ){
    if( mu < N ){
      vol *= len[mu];
      subv[mu] = 1;
      for( nu = mu+1; nu < N; nu++ ){
        subv[mu] *= len[nu];
      }
    }else{
      subv[mu] = 0;
    }
//========================TEST=================================
//      cout << "subv[" << mu << "]=" << subv[mu] << endl;
//=============================BLOCK===========================
  }

  return vol;
}

//==========================================================    
template< class T >
void multi1d< T >::restore(){
  
  int mu;

  N = ihr_N;
  volume = ihr_volume;

  for( mu = 0; mu < MAXDIM; mu++ ){
    length[mu] = ihr_length[mu];
    subvol[mu] = ihr_subvol[mu];
  }

  origin = 0;
}

//==========================================================    
template< class T >
void multi1d< T >::coalesce( int begin, int end ){

  int mu, nu, shift;

  shift = ( end - begin );
  N -= shift;

  for( mu = begin; mu < MAXDIM; mu++ ){

      if( mu == begin ){
        for( nu = begin+1; nu <= end; nu++ ){
          length[mu] *= length[nu];
        }
      }else if( (mu > begin) && (mu < N) ){
        length[mu] = length[mu+shift];
      }else{
        length[mu] = 0;
      }

  }
  
  volume = getVolume( length, subvol );

}

//==========================================================
// Obtain the subarray 
// by fixing the original tensor index(=mu) 
// to the value const_index.
//==========================================================
template< class T >
void multi1d< T >::subarray( int mu, int const_index ){

  int nu;

  N -= 1;
  origin += const_index*subvol[mu];
  volume /= length[mu];

  for( nu = mu; nu < MAXDIM; nu++ ){

      if( nu < N ){
        length[nu] = length[nu+1];
        subvol[nu] = subvol[nu+1];
      }else{
        length[nu] = 0;
        subvol[nu] = 0;
      }

  }

}

//==========================================================    
template< class T >
int multi1d< T >::offset( int i, int j, int k, int l, int m, int n ){

  int mu, location, index[MAXDIM];
  
  index[0] = i; 
  index[1] = j; 
  index[2] = k;
  index[3] = l;
  index[4] = m;
  index[5] = n;

  validate( index );

//========================TEST=================================
/*  cout << "( " << index[0] 
    << ", " << index[1]
    << ", " << index[2]
    << ", " << index[3]
    << ", " << index[4]
    << ", " << index[5] << " )" << endl; 
*/
//=============================BLOCK===========================

  location = origin;
  for( mu = 0; mu < N; mu++ ){
    location += ( index[mu] * subvol[mu] );
//========================TEST=================================
//    cout << "offset[" << mu << "]=" << location << endl;
//=============================BLOCK===========================
  }

  return location;
}

//==========================================================    
template< class T >
T multi1d< T >::get( int i, int j, int k, int l, int m, int n ){
  return *( matrix + offset(i,j,k,l,m,n) );
}

//==========================================================    
//template< class T >
//T multi1d< T >::get2d( int i, int j ){
//  return *( matrix + i*subvol[0] + j );
//}
//
//==========================================================    
template< class T >
void multi1d< T >::set( T value, int i, int j, int k, int l, int m, int n ){
  *( matrix + offset(i,j,k,l,m,n) ) = value;
}

//==========================================================    
//template< class T >
//void multi1d< T >::set1d( T value, int i ){
//  *( matrix + i ) = value;
//}
//
//==========================================================    
template< class T >
istream& multi1d< T >::set( istream& input, int i, int j, int k, int l, int m, int n ){
  input >> *( matrix + offset(i,j,k,l,m,n) );
  return input;
}

//==========================================================    
template< class T >
void multi1d< T >::zerod(){

    int i;

    if( volume == ihr_volume ){
      for( i = 0; i < volume; i++ ){
        *( matrix + i ) = 0;
      }
    }else{
      cerr << "zerod():: ERROR: Subarray can not be initialized by zero"
           << " using this function.\n You can do it manually." << endl;
      exit(1004);
    }

}

//==========================================================    
template< class T >
int multi1d< T >::dim(){
  return N;
}

//==========================================================    
template< class T >
int multi1d< T >::vol(){
  return volume;
}

//==========================================================    
template< class T >
int multi1d< T >::len( int i ){

  if( 0 <= i && i < N ){
    return length[i];
  }else{
    cerr << "ERROR: len()::Out of range" << endl;
    exit(1003);
  }

}

#endif
