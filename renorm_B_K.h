// "@(#) QCD: global.h v1.16 from 01/06/11 22:56:00"
//
//     anomalous dimension / renormalization constants for B_K.
//

//--------------------------------------
//     define c_TI = (4 * pi)^2 / 12
//--------------------------------------
//     define c_TI = c_F * I_MF (HYP II / FAT7 bar)
//------------------------------------------
      static const double c_F = 4.0 / 3.0 ;

#if defined HYP

      static const double c_TI = c_F * 1.05382 ;

#else

      static const double  c_TI = 13.15947252 ;

#endif

//--------------------------------------
//     define anomalous dimension
//--------------------------------------
      static const double gamma_vv1c = -4.0 ;
      static const double gamma_vv2c = -4.0 ;
      static const double gamma_aa1c = -4.0 ;
      static const double gamma_aa2c = -4.0 ;

//-------------------------------------------------
//     define continuum renormalization constants.
//-------------------------------------------------
      static const double cont_vv1c = -11.0/3.0 ;
      static const double cont_vv2c = -11.0/3.0 ;
      static const double cont_aa1c = -11.0/3.0 ;
      static const double cont_aa2c = -11.0/3.0 ;

//-------------------------------------------------
//     define lattice renormalization constants.
//-------------------------------------------------

#if defined HYP

      static const double latt_vv1c = -5.2994 + (2.0 * c_TI) + 0.3161                ;
      static const double latt_vv2c = -0.4170                -10.6903 + (4.0 * c_TI) ;
      static const double latt_aa1c = -5.8111 + (2.0 * c_TI) + 0.3161                ;
      static const double latt_aa2c = +1.1181                - 0.1052                ;

#else

      static const double latt_vv1c = -24.167 + (2.0 * c_TI) -  4.500                ;
      static const double latt_vv2c = - 7.028                - 58.523 + (4.0 * c_TI) ;
      static const double latt_aa1c = -24.768 + (2.0 * c_TI) -  4.500                ;
      static const double latt_aa2c = - 5.225                +  1.501                ;

#endif

//-------------------------------------------------
//    define renormalization constants.
//-------------------------------------------------

#if defined MIXED_ONE_LOOP

      static const double cnst_vv1c = -1.9458 ;
      static const double cnst_vv2c = +1.0627 ;
      static const double cnst_aa1c = -1.4757 ;
      static const double cnst_aa2c = -4.7549 ;

#else

      static const double cnst_vv1c = cont_vv1c - latt_vv1c ;
      static const double cnst_vv2c = cont_vv2c - latt_vv2c ;
      static const double cnst_aa1c = cont_aa1c - latt_aa1c ;
      static const double cnst_aa2c = cont_aa2c - latt_aa2c ;

#endif




