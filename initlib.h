//-----------------------------------------------------------------------------
// init.h
// 
//
// Declaration of fuctions used only in init_program() 
//
//-----------------------------------------------------------------------------

//----------------------------------------
// get user name, hostname and process id
//----------------------------------------
extern void get_user_info(void);

//-----------------
// create LockFile
//-----------------
extern void create_lockfile(void);

//----------------------------
// set flags and basic values
//----------------------------
extern void set_flags_n_values(void);

//-----------------------
// create error log file
//-----------------------
extern void create_err_log(void);

//-------------------------------------
// allocate memory and zero its memory
//-------------------------------------
extern void init_memory(void);

//---------------------------------
// Read lattice size, beta, kappa, 
// tmin, tmax, t_left, t_right,
// number of buffers
//---------------------------------
extern void  read_num_buf_lat_sz(void);

//----------------------------------------
// set variables for fitting and plotting
//----------------------------------------
extern void set_fit_variables(void);

//-------------------------------------
// Read label file
//
// b : index of buffers
// s : index of subbuffers
// c : index of channels
//-------------------------------------
extern void read_label_file(void);

//------------------------------------
// Read the  Number of Configurations
//------------------------------------
extern void ReadNcfg(void);

//-----------------------------
// Read Previous Analysis Data
//-----------------------------
extern void read_prev_anal(void);

//----------------------------------------------
// Read Previous Output File
//
// read parameters and jk information from file
//----------------------------------------------    
extern void ReadOutput(const char *filename);

//--------------------------------------
// select buffer, subbuffer and channel 
// values as zero
//--------------------------------------
extern void InitSelect(void);
