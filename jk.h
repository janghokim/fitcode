#ifndef __JK_H__
#define __JK_H__
//-----------------------------------------------------------------------------
// jk.h  
//             routines related with jk
//
// [24/08/2006] created by rvanguard
//
//-----------------------------------------------------------------------------

//----------------------------------------------
// Obtain jackKnife replications(mean, variace)
//
// del  : a observantion(cfg) will be deleted
// jkm  : JackKinfe replication of the mean
// jkv  : JackKinfe replication of the variance
//----------------------------------------------
extern void jk(int del, double *jkm, double *jkv); 

#endif // __JK_H__
