//-----------------------------------------------------------
// main_global.C :
//               define global variables and functions used
//               in functions : main_program, interrupt_point,
//               save_point and quit_point.
//
// 06/23/2005 [esrevinu] created.
//
//-----------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

#include "global.h"
#include "main_global.h"

//=======================================
// declarations of main_global variables
//=======================================

//-----------------------------------------------------------
// For processing the argument list in main_program function
//-----------------------------------------------------------
int     xargc;
char    **xargv;

//---------------------------------------------------
// In order to count the number of executed commands
//---------------------------------------------------
int nCom = 0;

//------------------------------------------------
// To check whether initialization be done or not
//------------------------------------------------
bool init_complete = false;

//------------------
// name of LockFile
//------------------
char72 LockFile;

//-------------------
// name of InputFile
//-------------------
char72 InputFile;

//-------------------
// name of OutputFile
//-------------------
char72 OutputFile;

//--------------------
// name of LabelFiles
//--------------------
char72 LabelFile;

//----------------------
// Full name of program
//----------------------
char128 fullprogname;

//---------------------------------
// Directory for saving plot files
//---------------------------------
char72 plotDir;

//------------------------------
// Infinite loop to make prompt
//------------------------------
bool loop = true;

//---------------------------------
// FILE pointer for error log file
//---------------------------------
FILE *fp_errlog;

//-------------------------
// FILE pointer input file
//-------------------------
FILE *fp_input;

//-------------------
// number of buffers
//-------------------
int nbuffer, ndbuffer, nlbuffer;

//-------------------
// For fitting range
//------------------
int n_1, n_2;


int t_1, t_2;
int x_1, x_2;
double y_1, y_2;

// For the fortran history
//char256 hisCom[9];
vector<string> hisCom;

//------------------------------------------------------------
// For a specific buffer, subbuffer and channel,
// we can calculate average and variance over the gauge conf. 
//------------------------------------------------------------
double avg[MAXNT], var[MAXNT * MAXNT];

double rat[MAXNT], erat[MAXNT];
double amp[MAXNT], eamp[MAXNT];
double avgx[MAXNT * MAXCFG];
double savedpars[MAXPAR], savederrs[MAXPAR], r0, r1, ro0, ro1;

//----------------------
// To check jk working
//----------------------
bool jkdone;

//----------------------------------------------------
// # of iterations svd, amoeba, and sing respectively
//----------------------------------------------------
int nsvd, namoeba, nsing;      

//======================================================
// declarations of functions being used in main_program
//======================================================

//--------------------------------
// process_argument_list function
//--------------------------------
void process_argument_list(int argc, char **argv)
{
  char *tmp_str;

  strncpy(fullprogname, argv[0], sizeof(fullprogname));

  //-----------------------------------------------
  // prog is set to the program name without path.
  //-----------------------------------------------
  if ((tmp_str = strrchr(fullprogname, '/')) != NULL) {
    strncpy(prog, tmp_str + 1, sizeof(prog));
  }
  else {
    strncpy(prog, fullprogname, sizeof(prog));
  }

  //------------------------------------------------------
  // To count the number of arguments except program name
  //------------------------------------------------------
  --argc;

  //------------------------------
  // Get input file from argument
  //------------------------------
  if (argc != 1) {
    printf("Usage : %s InputFile\n", prog);
    alert();
    exit(1);
  }
  else { 
    strncpy(InputFile, argv[1], sizeof(InputFile));
    printf("InputFile = %s\n", InputFile);
  }
}

//--------------------------------
// remove the lock file and *.pid
//--------------------------------
void remove_lock(void)
{ 
  char str[125];
  sprintf(str, "/bin/rm -f /tmp/*.%s", pid);
  system(str);
  sprintf(str, "/bin/rm -f %s", LockFile);
  system(str);
} 

