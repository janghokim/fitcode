//----------------------------------------------------------------------------
// commands.h
//
// Routines which are used to interact with user are declared.
//
// 05/08/2005 [rvanguard] created.
//
// RCS=$Id: commands.h,v 1.2 2015/08/26 09:10:34 jangho Exp $
//----------------------------------------------------------------------------

//------------------------------------------
// Quit the program after saving the result 
//------------------------------------------
extern int com_quit(char *arg);

//--------------------------------------------
// Quit the program without saving the result
//--------------------------------------------
extern int com_Quit(char *arg);

//------------------------------------
// Select a BUFFER.
// Average data for current selectin.
//------------------------------------
extern int com_buffer(char *arg);

//-------------------------------------
// Select a channel.
// Average data for current selection.
//-------------------------------------
extern int com_subbuffer(char *arg);

//-------------------------------------
// Select a channel.
// Average data for current selection.
//-------------------------------------
extern int com_channel(char *arg);

//-------------------------------------
// Select a subchannel.
// Average data for current selection.
//-------------------------------------
extern int com_subchannel(char *arg);

//------------------------------------
// fit the data with jackknife sample
//------------------------------------
extern int com_jk(char *arg);

//---------------------------------
// fit the data without resampling
//---------------------------------
extern int com_fit(char *arg);

//---------
// unfit 
//---------
extern int com_unfit(char *arg);

//-------------------------------
// Set or unset correlation flag
//-------------------------------
extern int com_corr(char *arg);

//--------------------------------------------------
// Make a one line note about the current channel
// -------------------------------------------------
extern int com_note(char *arg);

//------------------------------------------------------------------
// CHECKPOINT after cleaning up temporary files and dumping output
//------------------------------------------------------------------
extern int com_reload(char *arg);

//-------------------------
// Adjust parameters in minimizer
//-------------------------
extern int com_amoeba(char *arg);
extern int com_newton(char *arg);
extern int com_cg(char *arg);

//------------------------------
// SVD: adjust tolerance for svd
//------------------------------
extern int com_svd(char *arg);

//-------------------------------------------------
// MINIMIZER: read in the fitting routine MINIMIZER
//-------------------------------------------------
extern int com_MINIMIZER(char *arg);

//---------------------------------------
// fitlbl changes the type of fit locally
//---------------------------------------
extern int com_fitlbl(char *arg);

//-----------------------------------------------
// RSCH: read in the renormalization scheme Rsch
//-----------------------------------------------
extern int com_Rsch(char *arg);

//-----------------------------------------------------
// TEST: Print out a given configuration (ip) for this
//       buffer and channel to test if it is ok.
//-----------------------------------------------------
extern int com_test(char *arg);

//----------------------
// Set flags true/false
//----------------------
extern int com_set_flags(char *arg);

//-------------------------
// Unset flags true/false
//-------------------------
extern int com_unset_flags(char *arg);

//--------------------------
// SHOW : display variables
//--------------------------
extern int com_show(char *arg);

//-------------------------------
// Plot : various kinds of plots
//-------------------------------
extern int com_plot(char *arg);

//----------------------------------------
// MOVE: move a plot file to a safe place
//----------------------------------------
extern int com_move(char *arg);

//------------------------------------------------------------------
// CHECKPOINT after cleaning up temporarry files and dumping output 
//------------------------------------------------------------------
extern int com_checkpoint(char *arg);

//--------------------------------------------------------------------------------
// CHECKPOINTHEX : save the result as hexadecimal number to diff with other result
//--------------------------------------------------------------------------------
extern int com_checkpointhex(char *arg);

//----------------------------------------
// list all possible commands with usages
//----------------------------------------
extern int com_help(char *arg);

//-----------------------------------------
// commands queue for automatic executions 
//-----------------------------------------
extern int com_queue(char *arg);

//-----------------------------------------------
// set the command which will be executed by run
//-----------------------------------------------
extern int com_auto(char *arg);

//--------------------------------------------------
// print out fit parameters as hexadecimal numbers.
//--------------------------------------------------
extern int com_hex(char *arg);

//-----------------------------
// Set gsq to the input value
//-----------------------------
extern int com_gsq(char *arg);

//-----------------------------
// Set mua to the input value
//-----------------------------
extern int com_mua(char *arg);

//-----------------------------
// Set Unot to the input value
//-----------------------------
extern int com_Unot(char *arg);

//---------------------------------
// Set meff_ig to the input value
//---------------------------------
extern int com_meff_ig(char *arg);

//----------
// HISTORY
//----------
extern int com_history(char *arg);

//--------------------------------------------------------
// check to see if a previous command should be repeated
//--------------------------------------------------------
extern int com_repeat_1(char *arg);

//--------------------------------------------------------
// check to see if a previous command should be repeated
//--------------------------------------------------------
extern int com_repeat(char *arg);

//-------------------------------------------------------
// Send a command to the operating fsystem (10 args max)
//-------------------------------------------------------
extern int com_send(char *arg);

//-------------------------------------
// null commands are okay and ignored
//-------------------------------------
extern int com_null(char *arg);

//-----------------------------------------------------------
// allow arbitrary sequence starting with # to be a comment
//-----------------------------------------------------------
extern int com_comment(char *arg);
