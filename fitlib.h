#ifndef __FITLIB_H__
#define __FITLIB_H__
//-------------------------------------------------------------
// fitlib.h
//
// Routines for fitting parameters.
//
// RCS_id = $Id: fitlib.h,v 1.2 2015/08/26 09:10:34 jangho Exp $
//
// 07/01/2005 [esrevinu]: created.
//-------------------------------------------------------------

//-----------------------------------
// Get fitting range
// from arguments or previous values
//-----------------------------------
extern int GetFitRange(char *arg);


//--------------------------------------------------------------
// This routine fits the correlators by minimizing chi^2.
// . corflg = true  =>  the full variance matrix is used
// . corflg = false =>  correlations between data points are
// .                    neglected - variance matrix assumed
// .                    diagonal
// It calls the Numerical Recipes subroutines SVDCMP and AMOEBA.
//---------------------------------------------------------------
extern void corfit(int n1, int n2, const double *avg, const double *var,
                   double *actualc, double &cdf, double &q,
                   int &nsvd, int &nsing, int &namoeba);


//------------------------------------
// NumParameters
//
// Returns the number of parameters 
// corresponding to the fit type specified.  
//------------------------------------
extern int NumParameters(const char *fit);

//---------------------------------------------
// Evaluate the values of the fitting function
//---------------------------------------------
extern void f_value(const double c[], double y[],
		    const int n1, const int npt,
		    const char *fit);



//--------------------------------------------------------------
// Evaluate the value of the fitting function at timeslice = it
//--------------------------------------------------------------
extern double gfit(const char *fit, int it, const double c[MAXPAR]);
extern double gdfit(const char *fit, int it, const double c[MAXPAR], int a);
extern double gddfit(const char *fit, int it, const double c[MAXPAR], int a, int b);

//---------------------------------------
// Calculate Chi-Square for the fit data
//---------------------------------------
extern double ChiSquared(const double avgture[],
			  const double avgtmp[],
			  const double varinv[][MAXNT],
			  const int npt);

//-----------------------------------------------------------
// Calculate first derivative of Chi-Square for the fit data
//-----------------------------------------------------------
extern double dChiSquared(const double c[MAXPAR], const int n1,
        const int npt, const char *fit,
			  const double avgtmp[],
			  const double varinv[][MAXNT],
			  int a);

//-----------------------------------------------------------
// Calculate second derivative of Chi-Square for the fit data
//-----------------------------------------------------------
extern double ddChiSquared(const double c[MAXPAR], const int n1,
        const int npt, const char *fit,
			  const double avgtmp[],
			  const double varinv[][MAXNT],
			  int a, int b);
			  
//-------------------------
// Downhill Symplex Method
//-------------------------
extern void amoeba(double p[MAXPAR+1][MAXPAR], double y[MAXPAR+1], 
		   int ndim, int &iter, int npt, 
		   const double avgtmp[MAXNT],
                   const double varinv[][MAXNT]);

#endif
