#ifndef __DEBUG_H__
#define __DEBUG_H__
//-------------------------------------------------------------------
// debug.h
//
//-------------------------------------------------------------------

//----------------------------------------------------------
// debug_level = 1 ---> specific subroutine debugging
// debug_level = 10 ---> write the subroutine name
//----------------------------------------------------------
// debug_newt = debugging flag for the newt subroutine
//----------------------------------------------------------

#include <stdio.h>

extern int  debug_level;
extern int  debug_newt;
extern FILE *debug_fp;

//---------------------------------------------
// For hexadecimal precision check
//---------------------------------------------
typedef union single_real s_real;
typedef union double_real d_real;

union single_real { 
  float dec; 
  unsigned un; 
};

union double_real { 
  double dec;
  unsigned un[2]; 
};

extern s_real hex8;
extern d_real hex16;

//------------------------------------
// Subroutine setting the debug level
//------------------------------------
extern void setDebugFp(int level); 

//------------------------------
// fclose debug_fp FILE pointer
//------------------------------
extern void close_debug(void);

//-------------------------------------------------
// If debug levels are same, then print debug info 
//-------------------------------------------------
void Debug(int level, const char *fmt, ...);

#endif // __DEBUG_H__
