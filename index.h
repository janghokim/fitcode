#ifndef __INDEX_H__
#define __INDEX_H__
//----------------------------------------
// index.h
//
// Return indices for global variables.
// 
// RCS_id = $Id: index.h,v 1.1 2015/08/24 06:43:18 jangho Exp $
//
// 06/27/2005 [esrevinu]: created.
//----------------------------------------

extern int ind_buflbl(int i);
extern int ind_sublbl(int j, int i);
extern int ind_chalbl(int l, int k, int j, int i);
extern int ind_momlbl(int k, int j, int i);
extern int ind_fitlbl(int k, int j, int i);
extern int ind_fitMTR(int k, int j, int i);
extern int ind_modlbl(int k, int j, int i);
extern int ind_clbl(int i);
extern int ind_corlbl(int k, int j, int i);

extern int ind_note(int k, int j, int i);
extern int ind_clist(int l, int k, int j, int i);
extern int ind_frxx(int l, int k, int j, int i);
extern int ind_cxx(int l, int k, int j, int i);
extern int ind_cerrxx(int l, int k, int j, int i);
extern int ind_dxx(int l, int k, int j, int i);
extern int ind_derrxx(int l, int k, int j, int i);
extern int ind_cdfxx(int k, int j, int i);
extern int ind_qxx(int k, int j, int i);
extern int ind_cjkxx(int m, int l, int k, int j, int i);

extern int ind_cfgtbl(int j, int i);
extern int ind_nchin(int i);
extern int ind_nsub(int i);
extern int ind_nwsub(int i);
extern int ind_nchan(int j, int i);
extern int ind_ncfg(int i);
extern int ind_hdr(int k, int j, int i);
extern int ind_dat(int n, int m, int l, int k, int j, int i);

extern int ind_arg(int j, int i);
extern int ind_avg(int j, int i);
extern int ind_var(int j, int i);
extern int ind_avgx(int j, int i);
extern int ind_devx(int j, int i);

extern int ind_ntxcfg(const char *fname, int j, int i);

#endif // __INDEX_H__
