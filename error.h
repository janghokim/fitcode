#ifndef __ERROR_H__
#define __ERROR_H__
//-----------------------------------------------------------------------------
// error.h
//
//-----------------------------------------------------------------------------
#include <stdio.h>

//------------------------
// print WARNNING message
//------------------------
extern void alert(void);

//------------------------------------
// print error at fp and exit program
//------------------------------------
extern void ErrExit(const char *format, ...);

//---------------------
// print error message
//---------------------
extern void ErrMsg(const char *format, ...);

#endif // __ERROR_H__
