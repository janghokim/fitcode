#ifndef __READDATA_H__
#define __READDATA_H__
//-------------------------------------------------------------
// ReadData.h
//
// RCS_id = $Id: ReadData.h,v 1.1 2015/08/24 06:43:18 jangho Exp $
//
// 07/12/2005 [esrevinu]: created.
//-------------------------------------------------------------

//------------
// read datas
//------------
extern void ReadBuffer(void);

extern void read_eight(void);

#endif  // __READDATA_H__
