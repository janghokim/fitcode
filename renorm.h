//------------------------------------------------------
//  renorm.h
//
//  Feb 3, 2010. created by kwangwoo
//------------------------------------------------------
#if ( defined FIT_SWME || defined FIT_SWME_m1m2 )

extern void get_Z_B_K( double *Z_vv1c, double *Z_vv2c, double *Z_aa1c, double *Z_aa2c);
extern void msbar_B7_I2( double *Z_p_sq,
                  double *Z_pp1c, double *Z_pp2c, double *Z_ss1c, double *Z_ss2c, 
                  double *Z_vv1c, double *Z_vv2c, double *Z_aa1c, double *Z_aa2c);
void msbar_B8_I2( double *Z_p_sq,
                  double *Z_pp1c, double *Z_pp2c, double *Z_ss1c, double *Z_ss2c, 
                  double *Z_vv1c, double *Z_vv2c, double *Z_aa1c, double *Z_aa2c);
#endif
