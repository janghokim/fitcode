//------------------------------------------------------
// jk_b_k.C 
//
//  Jan 17, 2008. created by rvanguard
//------------------------------------------------------
#if (defined FIT_SWME_m1m2 && defined TREE_I2 )

#include <math.h>

#include "global.h"
#include "main_global.h"
#include "eight.h"

//---------------------------------------------
// VAC SAT analysis.
//  
//---------------------------------------------
void jk_B_K(int del, double *avg, double *devx, int &nc)
{

  double num[MAXNT];
  double num_aa[MAXNT];
  double num_vv[MAXNT];

  double numx[MAXNT*MAXCFG];
  double numx_aa[MAXNT*MAXCFG];
  double numx_vv[MAXNT*MAXCFG];

  int tmin = (tmin_read + nt) % nt;
  int tmax = (tmax_read + nt) % nt;
  if (tmax <= tmin) fputs("WARNING: tmax <= tmin\n", stderr);

  zero(num, MAXNT, double);
  zero(num_aa, MAXNT, double);
  zero(num_vv, MAXNT, double);
  zero(numx, MAXNT*MAXCFG, double);
  zero(numx_aa, MAXNT*MAXCFG, double);
  zero(numx_vv, MAXNT*MAXCFG, double);

  double norm = (ns * ns * ns) / 8;
  double vs_fact = (4.0/3.0) / norm;

  zero_eight();

  //--------------------
  // choose sub and cha
  //--------------------
  init_eight_cha();

  //--------------
  // jk.
  //--------------
  nc = 0;
  for (int i = 0; i < ncfg[buffer]; i++) {

    //------------------------
    // remove one observation
    //------------------------
    if (i == del) continue;


    for (int t = tmin; t <= tmax; t++) {
      //----------------------
      // get the data
      //----------------------
      get_eight_dat(t, i, nc);

      //----------------------
      // numerator    
      //----------------------
      numx[ind_numx(t,nc)] = eight_vv_1c[ind_eight_vv_1c(t,nc)]
                           + eight_vv_2c[ind_eight_vv_2c(t,nc)]
                           + eight_aa_1c[ind_eight_aa_1c(t,nc)]
                           + eight_aa_2c[ind_eight_aa_2c(t,nc)];

      //----------------------
      // AA   
      //----------------------
      numx_aa[ind_numx_aa(t,nc)] = eight_aa_1c[ind_eight_aa_1c(t,nc)]
                                 + eight_aa_2c[ind_eight_aa_2c(t,nc)];

      //----------------------
      // VV   
      //----------------------
      numx_vv[ind_numx_vv(t,nc)] = eight_vv_1c[ind_eight_vv_1c(t,nc)]
                                 + eight_vv_2c[ind_eight_vv_2c(t,nc)];
    }


    for (int t = tmin; t <= tmax; t++) {

      num[t] += numx[ind_numx(t,nc)];
      num_aa[t] += numx_aa[ind_numx_aa(t,nc)];
      num_vv[t] += numx_vv[ind_numx_vv(t,nc)];

      switch (channel) {
      case 0:                 // B_K 
        avgx[ind_avgx(t,i)] = numx[ind_numx(t,nc)] / 
	  ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 1:                 // VAC SAT
        avgx[ind_avgx(t,i)] = 
          a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact;
        break;
      case 2:                 // B_A1 / VS 
        avgx[ind_avgx(t,i)] = eight_aa_1c[ind_eight_aa_1c(t,nc)] / 
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 3:                 // B_A2 / VS
        avgx[ind_avgx(t,i)] = eight_aa_2c[ind_eight_aa_2c(t,nc)] /
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 4:                 // B_V1 / VS
        avgx[ind_avgx(t,i)] = eight_vv_1c[ind_eight_vv_1c(t,nc)] /
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 5:                 // B_V2 / VS
        avgx[ind_avgx(t,i)] = eight_vv_2c[ind_eight_vv_2c(t,nc)] /
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 6:                 // B_A / VS
        avgx[ind_avgx(t,i)] = numx_aa[ind_numx_aa(t,nc)] /
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      case 7:                 // B_V / VS
        avgx[ind_avgx(t,i)] = numx_vv[ind_numx_vv(t,nc)] /
          ( a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] * vs_fact );
        break;
      }
    }
    nc++;
  }

  for (int t = tmin; t <= tmax; t++) {
    //--------------------------------------
    // obtain the average over the data
    //--------------------------------------
    get_eight_avg(t, nc);

    num[t] /= nc;
    num_aa[t] /= nc;
    num_vv[t] /= nc;

    //-----------------------------------
    // construct individual channels
    //-----------------------------------
    switch (channel) {
    case 0:                     // B_K
      avg[t] = num[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact );
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = ( numx[ind_numx(t,i)]/num[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 1:                     // VAC SAT (B_K) 
      avg[t] =  sum_a0_l[t] * sum_a0_r[t] * vs_fact ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = ( a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          + a0_r[ind_a0_r(t,i)]/sum_a0_r[t] - 2 ) * avg[t]; 
      }
      break;
    case 2:                     // B_A1 / VS 
      avg[t] =  sum_eight_aa_1c[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( eight_aa_1c[ind_eight_aa_1c(t,i)] / sum_eight_aa_1c[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 3:                     // B_A2 / VS 
      avg[t] =  sum_eight_aa_2c[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( eight_aa_2c[ind_eight_aa_2c(t,i)] / sum_eight_aa_2c[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 4:                     // B_V1 / VS 
      avg[t] =  sum_eight_vv_1c[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( eight_vv_1c[ind_eight_vv_1c(t,i)] / sum_eight_vv_1c[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 5:                     // B_V2 / VS 
      avg[t] =  sum_eight_vv_2c[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( eight_vv_2c[ind_eight_vv_2c(t,i)] / sum_eight_vv_2c[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 6:                     // B_A / VS 
      avg[t] =  num_aa[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( numx_aa[ind_numx_aa(t,i)] / num_aa[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    case 7:                     // B_V / VS 
      avg[t] =  num_vv[t] / ( sum_a0_l[t] * sum_a0_r[t] * vs_fact ) ;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 
          ( numx_vv[ind_numx_vv(t,i)] / num_vv[t]
          - a0_l[ind_a0_l(t,i)]/sum_a0_l[t] 
          - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1 ) * avg[t]; 
      }
      break;
    default:
      fputs("jk_b_k: this channel is under construction\n", stderr);
      avg[t] = 0;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 1.0;
      }
      break;
    } // switch
  } // t
}
#endif
