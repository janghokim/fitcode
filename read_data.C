//------------------------------
// read_data.C
//
// 2015/08/24 [jangho] created.
//------------------------------
#include "global_H5.h"
#include "multi1d.h"
#include "Complex.h"
void read_data(multi1d<t_cmplx>& data, int b, int s, int c, int sc, int icnf){

  H5std_string set_name =
    conf_grp[b][s][c][sc][icnf].getObjnameByIdx(0);

  DataSet dataset = conf_grp[b][s][c][sc][icnf].openDataSet(set_name);
  DataSpace dataspace = dataset.getSpace();

  int num = dataspace.getSimpleExtentNpoints();

  CompType hdf5_cmplx_type(sizeof(t_cmplx)) ;
  hdf5_cmplx_type.insertMember("re", HOFFSET(t_cmplx, re), PredType::NATIVE_DOUBLE) ;
  hdf5_cmplx_type.insertMember("im", HOFFSET(t_cmplx, im), PredType::NATIVE_DOUBLE) ;
  t_cmplx *m_data_all = new t_cmplx[num]; // dataset for all timeslices

  dataset.read(m_data_all, hdf5_cmplx_type, dataspace) ;

  for( int i = 0 ; i < num ; i++ ){
    data.set(m_data_all[i], i);
  }
}
