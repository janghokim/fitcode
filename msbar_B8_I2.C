//==================================================================
// subroutine for B_K renormalization fators.
//==================================================================

#if ( defined FIT_SWME || defined FIT_SWME_m1m2 )

#include <math.h>
#include "global.h"
#include "renorm_B8_I2.h"

void msbar_B8_I2( double *Z_p_sq,
                  double *Z_pp1c, double *Z_pp2c, double *Z_ss1c, double *Z_ss2c, 
                  double *Z_vv1c, double *Z_vv2c, double *Z_aa1c, double *Z_aa2c)
{

  double Z_p;
  double a, x;

//--------------------------------
//     start code
//--------------------------------
      a = gsq / ((4.0 * pi) * (4.0 * pi));
      x = log( mua );

//---------------------------
//     bilinear
//---------------------------
      Z_p    =  1.0 +         a * ( gamma_p    * x + cnst_p    );
      *Z_p_sq =  1.0 + (2.0) * a * ( gamma_p    * x + cnst_p    );

//---------------------------
//    eight/current-current
//---------------------------
      *Z_pp1c =        a * ( gamma_pp1c * x + cnst_pp1c );
      *Z_pp2c =  1.0 + a * ( gamma_pp2c * x + cnst_pp2c );

      *Z_ss1c =        a * ( gamma_ss1c * x + cnst_ss1c );
      *Z_ss2c = -1.0 + a * ( gamma_ss2c * x + cnst_ss2c );

      *Z_vv1c =  1.0 + a * ( gamma_vv1c * x + cnst_vv1c );
      *Z_vv2c =        a * ( gamma_vv2c * x + cnst_vv2c );

      *Z_aa1c = -1.0 + a * ( gamma_aa1c * x + cnst_aa1c );
      *Z_aa2c =        a * ( gamma_aa2c * x + cnst_aa2c );

//----------------
//     All done
//----------------

}

#endif
