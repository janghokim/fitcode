// "@(#) QCD: global.h v1.16 from 01/06/11 22:56:00"
//
//     anomalous dimension / renormalization constants for B_8.
//

//--------------------------------------
//     define c_TI = (4 * pi)^2 / 12
//--------------------------------------------------
//     define c_TI = c_F * I_MF (HYP II / FAT7 bar)
//--------------------------------------------------
      static const double c_F  = 4.0 / 3.0  ;

#if defined HYP

      static const double c_TI = c_F * 1.05382  ;

#else

      static const double c_TI = 13.15947252 ;

#endif

//--------------------------------------
//     define anomalous dimension
//--------------------------------------
      static const double gamma_ss1c =   0   ;
      static const double gamma_ss2c = -16.0 ;
      static const double gamma_pp1c =   0   ;
      static const double gamma_pp2c = +16.0 ;

      static const double gamma_vv1c = +16.0 ;
      static const double gamma_vv2c =   0   ;
      static const double gamma_aa1c = -16.0 ;
      static const double gamma_aa2c =   0   ;

      static const double gamma_p = 8 ;

//-------------------------------------------------
//     define continuum renormalization constants.
//-------------------------------------------------
      static const double cont_ss1c = + 3.0     ;
      static const double cont_ss2c = -23.0/3.0 ;
      static const double cont_pp1c = - 3.0     ;
      static const double cont_pp2c = +23.0/3.0 ;

      static const double cont_vv1c = +23.0/3.0 ;
      static const double cont_vv2c = - 3.0     ;
      static const double cont_aa1c = -23.0/3.0 ;
      static const double cont_aa2c = + 3.0     ;

      static const double cont_p = +10.0/3.0;

//-------------------------------------------------
//     define lattice renormalization constants.
//----------------------------------------------------------
//     O_8       = - ( [SxP][SxP]_II - [PxP][PxP]_II )
//         + (1/2) * ( [VxP][VxP]_I  - [AxP][AxP]_I  )
//
//     vac sat = < PxP > < PxP > - (1/6) * < AxP > < AxP > 
//----------------------------------------------------------

#if defined HYP

      static const double latt_ss1c =    0                    ;
      static const double latt_ss2c = + 19.118 - (6.0 * c_TI) ;
      static const double latt_pp1c =    0                    ;
      static const double latt_pp2c = +  6.925 - (2.0 * c_TI) ;

      static const double latt_vv1c = - 6.0369 + (2.0 * c_TI) ;
      static const double latt_vv2c = - 0.7331                ;
      static const double latt_aa1c = + 6.5486 - (2.0 * c_TI) ;
      static const double latt_aa2c = - 0.8020                ;

      static const double latt_p    = + 3.465  - (1.0 * c_TI) ;

#else

      static const double latt_ss1c =    0                    ;
      static const double latt_ss2c = + 95.607 - (6.0 * c_TI) ;
      static const double latt_pp1c =    0                    ;
      static const double latt_pp2c = +111.255 - (2.0 * c_TI) ;

      static const double latt_vv1c = -13.668 + (2.0 * c_TI) ;
      static const double latt_vv2c = - 2.529                ;
      static const double latt_aa1c = +14.269 - (2.0 * c_TI) ;
      static const double latt_aa2c = + 0.725                ;

      static const double latt_p    = +55.628 - (1.0 * c_TI) ;

#endif

//-------------------------------------------------
//     define renormalization constants.
//-------------------------------------------------
      static const double cnst_ss1c = cont_ss1c - latt_ss1c ;
      static const double cnst_ss2c = cont_ss2c - latt_ss2c ;
      static const double cnst_pp1c = cont_pp1c - latt_pp1c ;
      static const double cnst_pp2c = cont_pp2c - latt_pp2c ;

      static const double cnst_vv1c = cont_vv1c - latt_vv1c ;
      static const double cnst_vv2c = cont_vv2c - latt_vv2c ;
      static const double cnst_aa1c = cont_aa1c - latt_aa1c ;
      static const double cnst_aa2c = cont_aa2c - latt_aa2c ;

      static const double cnst_p    = cont_p    - latt_p    ;




