//-------------------------------------------------------------------
// debug.C
//
//-------------------------------------------------------------------

//----------------------------------------------------------
// debug_level = 1 ---> specific subroutine debugging
// debug_level = 10 ---> write the subroutine name
//----------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "debug.h"
 
int  debug_level;
int  debug_newt;
FILE *debug_fp; 

s_real hex8;
d_real hex16;

//------------------------------------
// Subroutine setting the debug level
//------------------------------------
void setDebugFp(int level) 
{
  debug_level = level;

  if (level > 0) {
    debug_fp = fopen("C_debug.log", "w");
    if (!debug_fp) {
      fprintf(stderr, "file open error : C_debug.log\n");
      exit(1);
    }
    fprintf(debug_fp, 
	    "=== DEBUG INFO : debug_level : %d ===\n",
	    debug_level);

  }
  return;
}

//------------------------------
// fclose debug_fp FILE pointer
//------------------------------
void close_debug(void)
{
  if (debug_level > 0) fclose(debug_fp);
  return;
}

//-------------------------------------------------
// If debug levels are same, then print debug info 
//-------------------------------------------------
void Debug(int level, const char *fmt, ...)
{
  if (debug_level == level) {
    va_list ap;
    va_start(ap, fmt); 
    vfprintf(debug_fp, fmt, ap);
    va_end(ap);
    return;
  }
  else return;
}
