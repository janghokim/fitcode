//-----------------------------------------------------------------------------
// fitftns.h
//           - Fitting functions and those derivatives
//   
// [04/09/2006] created by rvanguard
//-----------------------------------------------------------------------------

//--------------------------------------------
// global variables used in fitting functions  
//--------------------------------------------
extern double data[4];
extern int fit_tt, fit_ntt;
extern char FitType[24];

//-------
// fcosh
//-------
extern double fcosh(double m, int t, int nt);
extern double fcosh_v2(double m, int t, int nt);

//---------------------------------------
// dfcosh - derivative of fcosh w.r.t. m
//---------------------------------------
extern double dfcosh(double m, int t, int nt);
extern double dfcosh_v2(double m, int t, int nt);

//-------
// fsinh
//-------
extern double fsinh(double m, int t, int nt);
extern double fsinh_v2(double m, int t, int nt);

//---------------------------------------
// dfsinh - derivative of fsinh w.r.t. m
//---------------------------------------
extern inline double dfsinh(double m, int t, int nt);
extern inline double dfsinh_v2(double m, int t, int nt);

//---------------------------
// funcv() : fitting fuction
//---------------------------
extern void funcv(int n, double *x, double *fvec);

//-------------------
// Analytic Jacobian
//-------------------
extern int Jac(int n, double x[], double **df);
