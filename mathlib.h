//-----------------------------------------------------------------------------
// mathlib.h
//
//
//-----------------------------------------------------------------------------
extern void InvMes(const double * indat, double * outdat, int t,
                   int nt2, double & ff);
extern void InvMes_Amoeba(const double * indat, double * outdat, int t,
                          int nt2, double & ff);
extern void fitmes(const char *fit, int t, int nt, int n,
                   const double * dat, double scale, double * par);
