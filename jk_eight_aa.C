//------------------------------------------------------
//  jk_eight_aa.C
//
//  Dec 16, 2007. created by rvanguard
//------------------------------------------------------
#if ( defined FIT_SWME_m1m2 && defined NAIVE )

#include <math.h>

#include "global.h"
#include "main_global.h"
#include "eight.h"

//---------------------------------------------
// VAC SAT analysis
//  
//---------------------------------------------
void jk_eight_aa(int del, double *avg, double *devx, int &nc)
{
  double den[MAXNT];

  int tmin = (tmin_read + nt) % nt;
  int tmax = (tmax_read + nt) % nt;
  if (tmax <= tmin) fputs("WARNING: tmax <= tmin\n", stderr);

  double norm = (ns * ns * ns) / 8;
  double aa_norm, pp_norm;

  zero_eight();

  //--------------------
  // choose sub and cha
  //--------------------
  init_eight_cha();

  //--------------
  // jk.
  //--------------
  nc = 0;
  for (int i = 0; i < ncfg[buffer]; i++) {

    //------------------------
    // remove one observation
    //------------------------
    if (i == del) continue;


    //----------------------
    //     get the data
    //----------------------
    for (int t = tmin; t <= tmax; t++) {
      get_eight_dat(t, i, nc);
    }

    for (int t = tmin; t <= tmax; t++) {

      //--------------------------------------------------
      // obtain the denomenator: <K| A_0 |0> <0| A_0 |pi>
      //--------------------------------------------------
      aa_norm = a0_l[ind_a0_l(t,nc)] * a0_r[ind_a0_r(t,nc)] / norm;
      pp_norm = p_l[ind_p_l(t,nc)] * p_r[ind_p_r(t,nc)] / norm;


      switch (channel) {
        case 0:                 // SS1C eight / AA
          avgx[ind_avgx(t,i)] = eight_ss_1c[ind_eight_ss_1c(t,nc)] / aa_norm;
          break;
        case 1:                 // PP1C eight / AA
          avgx[ind_avgx(t,i)] = eight_pp_1c[ind_eight_pp_1c(t,nc)] / aa_norm;
          break;
        case 2:                 // VV1C eight / AA
          avgx[ind_avgx(t,i)] = eight_vv_1c[ind_eight_vv_1c(t,nc)] / aa_norm;
          break;
        case 3:                 // AA1C eight / AA
          avgx[ind_avgx(t,i)] = eight_aa_1c[ind_eight_aa_1c(t,nc)] / aa_norm;
          break;
        case 4:                 // SS2C eight / AA
          avgx[ind_avgx(t,i)] = eight_ss_2c[ind_eight_ss_2c(t,nc)] / aa_norm;
          break;
        case 5:                 // PP2C eight / AA
          avgx[ind_avgx(t,i)] = eight_pp_2c[ind_eight_pp_2c(t,nc)] / aa_norm;
          break;
        case 6:                 // VV2C eight / AA
          avgx[ind_avgx(t,i)] = eight_vv_2c[ind_eight_vv_2c(t,nc)] / aa_norm;
          break;
        case 7:                 // AA2C eight / AA
          avgx[ind_avgx(t,i)] = eight_aa_2c[ind_eight_aa_2c(t,nc)] / aa_norm;
          break;
        case 8:                 // AA/PP
          avgx[ind_avgx(t,i)] = aa_norm / pp_norm;
          break;
      }
    }
    nc++;
  }

  for (int t = tmin; t <= tmax; t++) {
    //--------------------------------------
    // obtain the average over the data
    //--------------------------------------
    get_eight_avg(t, nc);

    //-----------------------------------
    // construct individual channels
    //-----------------------------------
    den[t] = sum_a0_l[t] * sum_a0_r[t] / norm;

    //-----------------------------------
    // construct individual channels
    //-----------------------------------
    switch (channel) {
    case 0:                     // SS1C eight / AA
      avg[ind_avg(t,0)] = sum_eight_ss_1c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_ss_1c[ind_eight_ss_1c(t,i)]/sum_eight_ss_1c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 1:                     // PP1C eight / AA
      avg[ind_avg(t,0)] = sum_eight_pp_1c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_pp_1c[ind_eight_pp_1c(t,i)]/sum_eight_pp_1c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 2:                     // VV1C eight / AA
      avg[ind_avg(t,0)] = sum_eight_vv_1c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_vv_1c[ind_eight_vv_1c(t,i)]/sum_eight_vv_1c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 3:                     // AA1C eight / AA
      avg[ind_avg(t,0)] = sum_eight_aa_1c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_aa_1c[ind_eight_aa_1c(t,i)]/sum_eight_aa_1c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 4:                     // SS2C eight / AA
      avg[ind_avg(t,0)] = sum_eight_ss_2c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_ss_2c[ind_eight_ss_2c(t,i)]/sum_eight_ss_2c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 5:                     // PP2C eight / AA
      avg[ind_avg(t,0)] = sum_eight_pp_2c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_pp_2c[ind_eight_pp_2c(t,i)]/sum_eight_pp_2c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 6:                     // VV2C eight / AA
      avg[ind_avg(t,0)] = sum_eight_vv_2c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_vv_2c[ind_eight_vv_2c(t,i)]/sum_eight_vv_2c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 7:                     // AA2C eight / AA
      avg[ind_avg(t,0)] = sum_eight_aa_2c[t] / den[t];
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (eight_aa_2c[ind_eight_aa_2c(t,i)]/sum_eight_aa_2c[t]
             - a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t] + 1) * avg[ind_avg(t,0)];
      }
      break;
    case 8:                     // AA/PP
      avg[ind_avg(t,0)] = den[t] / (sum_p_l[t] * sum_p_r[t] / norm);
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)]
          = (a0_l[ind_a0_l(t,i)]/sum_a0_l[t]
             - a0_r[ind_a0_r(t,i)]/sum_a0_r[t]
             + p_l[ind_p_l(t,i)]/sum_p_l[t]
             + p_r[ind_p_r(t,i)]/sum_p_r[t]) * avg[ind_avg(t,0)];
      }
      break;
    default:
      fputs("jk_eight: under construction\n", stderr);
      avg[t] = 0;
      for (int i = 0; i < nc; i++) {
        devx[ind_devx(t,i)] = 1.0;
      }
      break;
    } // switch
  } // t
}
#endif
