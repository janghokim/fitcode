#ifndef __COMMANDLIB_H__
#define __COMMANDLIB_H__
#include <readline/readline.h>
#include <readline/history.h>
//-------------------------------------------------------------
// commandlib.h
// 
// Routines for the user interface of program fit.
// The first routine get a command + arguments, the
// remaining routines are for specific commands.
//
// RCS_id = $Id: commandlib.h,v 1.2 2015/08/26 09:10:34 jangho Exp $
//
// 07/01/2005 [esrevinu]: created.
//-------------------------------------------------------------

//----------------------------------------------------------------
// A structure which contain the name of commands and those usage
//----------------------------------------------------------------
typedef struct {
  char *name;           // The name of command
  rl_icpfunc_t *func;   // Function to call to do the job.
  char *doc;            // Documentation for this function 
} COMMAND;

extern COMMAND commands[];

//-------------------
// Execute a command
//-------------------
extern int exec(char *line);

//---------------------------------------------------------------------
// Look up NAME as the name of a command, and return a pointer to that
// command.  Return a NULL pointer if NAME isn't a command name. 
//---------------------------------------------------------------------
extern COMMAND *find_command (char *name);

//-----------------------------------------------
// Selects buffer, subbuffer and channel at once
//-----------------------------------------------
extern void SelectBSC(int b, int s, int c, int sc);

//------------------------
// Show available buffers
//------------------------
extern void ShowAvailBuf(void);

//---------------------------
// Show available subbuffers
//--------------------------
extern void ShowAvailSubbuf(void);

//-------------------------
// Show available channels
//-------------------------
extern void ShowAvailChan(void);

//-------------------------
// Show available channels
//-------------------------
extern void ShowAvailSubcha(void);

//------------------------------
// Show available configurations
//------------------------------
extern void ShowAvailConf(char256 argu);

//-------------------------------------------------------
// show the information about buffer, subbuf and channel
//-------------------------------------------------------
extern void ShowBSC(void);

//---------------------------------
// Show the current value of flags
//---------------------------------
extern void ShowFlags();

//-------------------------------------------
// Show the average and variance of the data
//-------------------------------------------
extern void ShowData();

//---------------------------------------
// List the configurations in the sample
//---------------------------------------
extern void ShowSample();

//-------------------------------------------------------
// Show parameters of the latest fit for this channel.
//-------------------------------------------------------
extern void ShowParameters(int m1, int m2, int n1, int n2);

//----------------------------------------
// Fit Parameters.
// fit to the specified number of points.
//----------------------------------------
extern void FitParameters(int n1, int n2, const double * avg,
                          const double * var, int &nsvd, int &nsing,
                          int &nameoba);
//--------------------
// Fixes parameters.
//--------------------
extern void FixParameters(int iflag);

//---------------------------------------------
// Interactive adustment of minimizer parameters.
//---------------------------------------------
extern void AdjustAmoeba();
extern void AdjustNewton();
extern void AdjustCg();

//-----------------------------------------
// Interactive adustment of svd tolerance.
//-----------------------------------------
extern void AdjustSvd();

//--------------------------------------
// Fit Parameters with jackknife sample
//--------------------------------------
extern void JackKnife(int n1, int n2, double * rat, double * erat,
                      double * amp, double * eamp);
//----------------------------
// Set or Unset Various Flags
//----------------------------
extern void SetOrUnsetFlag(const char *str1, const char *str2);

//----------------------------------------------
// write an informative stamp to io unit i
//----------------------------------------------
extern void WriteStamp(FILE * fp, const char *prog,
                       const char *user, const char *host) throw(char *);

//-------------------------------------------------------
// dump current parameters and jk information to file
//-------------------------------------------------------
extern void WriteOutput(const char *filename) throw(char *);

//----------------------------------------------------------------------------------
// dump current parameters and jk information as hexadecimal number to binary check
//----------------------------------------------------------------------------------
extern void WriteOutputHex(const char *filename) throw(char *);

extern void TestPropagator(int icfg, double *avgx);

#endif
