//----------------------------------------------------------------------------
// plotlib.h
//
// 08/08/2005 [rvanguard] : created.
//
// RCS=$Id: plotlib.h,v 1.3 2015/08/31 07:01:06 jangho Exp jangho $
//----------------------------------------------------------------------------

#ifndef __PLOTLIB_H__
#define __PLOTLIB_H__
//-------------------------
// plot the data vs t_slice
//-------------------------
extern void PlotData(string dir, int is_real);

//-------------------------------------------------------
//  plot value of correlator at time slice t vs. configs
//-------------------------------------------------------
extern void PlotAcor(int t, const double *avgx);

//------------------------------------------------
//  Plot correlator data and fit to ts = [n1, n2]
//------------------------------------------------
extern void PlotCor(int n1, int n2, const double *avg, const double *var);

//-----------------------------------------
//  Calculate and plot effective amplitude
//-----------------------------------------
extern void PlotAmp(int n1, int n2, int t1, int t2, int x1, int x2,
                    double y1, double y2, const double * amp,
                    const double * eamp);

//----------------------------------------------------
//  Calculate and plot effective mass (or equivalent)
//----------------------------------------------------
extern void PlotRatio(int n1, int n2, int t1, int t2, int x1, int x2,
                      double y1, double y2, const double * rat,
                      const double * erat);

//----------------------------------------------------
//  Calculate and plot effective mass (or equivalent)
//----------------------------------------------------
extern void PlotRRatio(int n1, int n2, int t1, int t2, int x1, int x2,
		       double y1, double y2, const double * rat,
		       const double * erat);

extern void move(char *PlotType, char *file);

//-------------------
// Adjust plot range
//-------------------
extern int AdjustPlotRange(char256 *argu);

#endif
