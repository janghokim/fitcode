//-----------------------------------------------------------------------------
// readlib.h
//
// Subroutines to read files or inputs
//
//-----------------------------------------------------------------------------

//----------------------------
// read 'y' or 'n' from stdin
//----------------------------
extern int ReadChar(char *input);

//--------------------------------------------
// read one line from FILE using scanf format 
//--------------------------------------------
extern void ReadLine(FILE *fp, const char *filename, const char *fmt, ...);

//--------------------------------------------------------------------
// Sscanf is analogous to sscanf, but it change the pointer of string
// to make one easy read the next input by next calling.
//--------------------------------------------------------------------
extern int Sscanf(char *str, const char *fmt, ...);
