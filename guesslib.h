#ifndef __GUESS_H__
#define __GUESS_H__
//-----------------------------------------------------------------------------
// guesslib.h
// 
//      Subroutines using when the initial guess for fitting is needed
//
// [28/08/2006] created by rvanguard
//-----------------------------------------------------------------------------
#include "global.h"

//----------------------------------------------------
// Given data in avg(0:nt), guess fit parameters by
// drawing curve through 4 pts t, ..., t+3
// Functional form of fit specified by fitlbl.
//----------------------------------------------------
extern void guess(const double * avg, int t);

//--------------------------------------------------
// Obtain parameters by using newton-raphson method 
//--------------------------------------------------
extern void fitmes(const char *fit, const int t, const int nt, const int n, 
		   const double *InDat, const double scale, double *Para);

#endif  //__GUESS_H__
