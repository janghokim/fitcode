#ifndef __STAT_H__
#define __STAT_H__
#include "Complex.h"
#include "multi1d.h"

void avg_err(t_cmplx& avg, t_cmplx& err, multi1d<t_cmplx>& data, int nconf ) ;
#endif
