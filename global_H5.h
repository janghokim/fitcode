#ifndef __GLOBAL_H5_H__
#define __GLOBAL_H5_H__
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "H5Cpp.h"

using namespace std;
using namespace H5; // hdf5

extern Group *buf_grp ;
extern int num_buf_grp ;
extern H5std_string *name_buf_grp ;

extern Group **subbuf_grp ;
extern int *num_subbuf_grp ;
extern H5std_string **name_subbuf_grp ;

extern Group ***cha_grp ;
extern int **num_cha_grp ;
extern H5std_string ***name_cha_grp ;

extern Group ****subcha_grp ;
extern int ***num_subcha_grp ;
extern H5std_string ****name_subcha_grp ;

extern Group *****conf_grp ;
extern int ****num_conf_grp ;
extern H5std_string *****name_conf_grp ;

extern int *****num_data ;
#endif
