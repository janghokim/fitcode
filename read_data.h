#ifndef __READ_DATA_H__
#define __READ_DATA_H__
#include "global_H5.h"
#include "multi1d.h"
#include "Complex.h"
void read_data(multi1d<t_cmplx>& data, int b, int s, int c, int sc, int icfg) ;
#endif
