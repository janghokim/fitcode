//------------------------------------------------------
//  jklib.h
//
//  Dec 3, 2007. created by rvanguard
//------------------------------------------------------
#include "global.h"

#if ( defined FIT_SWME_m1m2 && defined NAIVE )
extern void jk_mass(int del, double *avg, double *devx, int &nc);
extern void jk_eight(int del, double *avg, double *devx, int &nc);
extern void jk_eight_aa(int del, double *avg, double *devx, int &nc);


#elif ( defined FIT_SWME_m1m2 && defined TREE_I2 )
extern void jk_B_K(int del, double *avg, double *devx, int &nc);
extern void jk_B7_I2(int del, double *avg, double *devx, int &nc);
extern void jk_B8_I2(int del, double *avg, double *devx, int &nc);

#elif ( (defined FIT_SWME || defined FIT_SWME_m1m2) && defined RENORM_I2 )
extern void jk_BK_msbar(int del, double *avg, double *devx, int &nc);
extern void jk_B7_I2_msbar(int del, double *avg, double *devx, int &nc);
extern void jk_B8_I2_msbar(int del, double *avg, double *devx, int &nc);
extern void jk_BK_msbar_g4(int del, double *avg, double *devx, int &nc);

#endif
