#ifndef COMPLEX_H
#define COMPLEX_H
#include <iostream>
#include <string>
using namespace std;
// complex struct for hdf5 save
typedef struct t_cmplx
{
  double re;
  double im;

  t_cmplx()
  {
    re = 0.0;
    im = 0.0;
  }
} t_cmplx;

// struct containing source-wise data
typedef struct t_data 
{
  t_cmplx *m_data; // Data
  string src;      // soruce position
  string prec;     // LP or HP
  int size;        // Number of m_data elements
} t_data;

#endif
