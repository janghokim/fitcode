//-----------------------------------------
//                                         
//    Interface to Readline Completion     
//                                         
//-----------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "global.h"
#include "completion.h"
#include "commandlib.h"

//----------------------------------------------------------------
// Tell the GNU Readline library how to complete.  
// We want to try to complete on command names 
// if this is the first word in the line, or on filenames if not. 
//----------------------------------------------------------------
void initialize_readline (void)
{
    // Allow conditional parsing of the ~/.inputrc file.
    rl_readline_name = "SPECFIT";

    // Tell the completer that we want a crack first.
    rl_attempted_completion_function = specfit_completion;

    read_history ("history_file");
}

//----------------------------------------------------------------
// Attempt to complete on the contents of TEXT.  START and END bound the
// region of rl_line_buffer that contains the word to complete.  TEXT is
// the word to complete.  We can use the entire contents of rl_line_buffer
// in case we want to do some simple parsing.  Return the array of matches,
// or NULL if there aren't any.
//----------------------------------------------------------------
char ** specfit_completion (const char *text, int start, int end)
{
  char **matches;

  matches = (char **)NULL;

  //---------------------------------------------------------
  // If this word is at the start of the line, then it is a command
  // to complete.  Otherwise it is the name of a file in the current
  // directory. 
  //---------------------------------------------------------
  // if (start == 0)
  //   matches = rl_completion_matches (text, command_generator);
  //---------------------------------------------------------

  //--------------------------------------------------------------
  // When I also want command completion when we are not at start 
  //--------------------------------------------------------------
  matches = rl_completion_matches (text, command_generator);

  return (matches);
}

//-------------------------------------------------------------------------
// Generator function for command completion.  STATE lets us know whether
// to start from scratch; without any state (i.e. STATE == 0), then we
// start at the top of the list. 
//-------------------------------------------------------------------------
char * command_generator (const char *text, int state)
{
  static int list_index, len;
  char *name;

  // If this is a new word to complete, initialize now.  This includes
  // saving the length of TEXT for efficiency, and initializing the index
  // variable to 0. 
  if (!state)
    {
      list_index = 0;
      len = strlen (text);
    }

  /* Return the next name which partially matches from the command list. */
  while ((name = commands[list_index].name))
    {
      list_index++;

      if (strncmp (name, text, len) == 0)
        return (dupstr(name));
    }

  /* If no names matched, then return NULL. */
  return ((char *)NULL);
}

char * dupstr (char *s)
{
  char *r;

  r = (char *)malloc (strlen (s) + 1);
  strcpy (r, s);
  return (r);
}
