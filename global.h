#ifndef __GLOBAL_H__
#define __GLOBAL_H__
//----------------------------------------
// global.h
//
// Public variables are defined.
// 
// RCS_id = $Id: global.h,v 1.2 2015/08/26 09:10:34 jangho Exp $
//
// 06/21/2005 [esrevinu]: created.
//----------------------------------------

//----------------------------------
// Declaration of global data types
//----------------------------------
typedef char    char8[8];
typedef char    char16[16];
typedef char    char24[24];
typedef char    char32[32];
typedef char    char64[64];
typedef char    char72[72];
typedef char    char78[78];
typedef char    char128[128];
typedef char    char256[256];

// The boolean type is in C++. But reading by scanf() is difficult.
typedef int     BOOL;		// zero : false, not zero: true

//--------------
// pi
//--------------
extern double pi;

//------------------------------------------------
// program name, user name, host name, process id 
//------------------------------------------------
extern char     prog[24], user[24], host[24], pid[5];

//---------------
// name of PAGER
//---------------
extern char    *pager;

//-------------------------------------------------
// indice for channel, subbuffer, buffer and itype
//-------------------------------------------------
extern int subcha, channel, subbuf, buffer, itype;

//--------------
// lattice size
//--------------
extern int      ns, nt;

extern double  beta, gsq, kappa[MAXKAP], mua, Unot, c_A, c_V; // real
extern char     Rsch[8];          // Rsch[8];


// data ( <- label in fortran code )
extern int      cfgtbl[MAXCFG*MAXBUF];

//----------------------
// number of subbuffers
//----------------------
extern int      nsub[MAXBUF];
extern int      nwsub[MAXBUF];

//--------------------------------------
// number of channels of each subbuffer
//--------------------------------------
extern int      nchan[MAXSUB*MAXBUF];

//--------------------------
// number of configurations
//--------------------------
extern int      ncfg[MAXBUF];

extern int      nhdr;
extern int      nchin[MAXBUF];

extern double  hdr[20*MAXCFG*MAXBUF];


//-----------
// Main DATA
//-----------

#define         NREAL         (2)
#define         I_REAL        (0)
#define         I_IMAG        (1)
extern double  *dat;

#define         STRLEN          255
extern char     eight_dir[STRLEN];

// indices for subbuffers
#define         BI_OP_LEFT      0
#define         BI_OP_RIGHT     1
#define         FF1C            3
#define         FF2C            5

#define         EIGHT_OFFSET    0


//---------------
// buffer labels
//---------------
extern char78   buflbl[MAXBUF];

//------------------
// subbuffer labels
//------------------
extern char128  sublbl[MAXSUB*MAXBUF];

//----------------
// channel labels
//----------------
extern char32   chalbl[2*MAXCHA*MAXSUB*MAXBUF];

//------------------
// subchannel labels
//------------------
extern char32   subchalbl[2*MAXCHA*MAXSUB*MAXBUF];

//-----------------
// momentum labels
//-----------------
extern char16   momlbl[MAXCHA*MAXSUB*MAXBUF];

//-----------------
// fit-type labels
//-----------------
extern char24   fitlbl[MAXCHA*MAXSUB*MAXBUF];
extern char24   fitMTR[MAXCHA*MAXSUB*MAXBUF];

extern char8    clbl[MAXPAR];
extern char     corlbl[MAXCHA*MAXSUB*MAXBUF];

// fit ( <- label in fortran code )
extern char64   *note;          
extern char256  *queue;         
extern char256  *autoCom;         
extern int      *clist;         
extern int      *frxx;      
extern double  *cxx;           
extern double  *cerrxx;        
extern double  *dxx;           
extern double  *derrxx;        
extern double  *cdfxx;         
extern double  *qxx;           
extern double  *cjkxx;         


// flags ( <- label in fortran code )
extern BOOL     corflg;
extern BOOL     symflg;
extern BOOL     flpflg;
extern BOOL     guessflg;
extern BOOL     ampflg;
extern BOOL     rrflg;
extern BOOL     verboseflag;
extern BOOL     varout;

extern BOOL     forceflag;
extern BOOL     subchafix;
extern BOOL     channelfix;
extern BOOL     subbuffix;
extern BOOL     bufferfix;

extern BOOL     showPlotFlag;
extern BOOL     savePlotFlag;
//-------------------------
// For stag meson spectrum
// to fit oscillating data
//-------------------------
extern BOOL     OscilFlag;

//--------------
// plot program
//--------------
extern char     plotcommand[256];   // char [255] : fortran

// force ( <- label in fortran code )
extern char     forcecorr;

// fitRT ( <- label in fortran code )
extern char     *MINIMIZER;     // char [24] : fortran
extern char     *MIN_AN;        // char [7]  : fortran
        
// AdjustableParameters ( <- label in fortran code )
extern int      MaxitAmoeba, MaxitNewton, MaxitCG;
extern int      maxmode;
extern double  TolAmoeba, TolNewton, TolCG;
extern double  SvdTol;
extern double  StartFudge[MAXPAR];


// misc ( <- label in fortran code )
extern BOOL     a4flag;
extern int      ffdt;
extern int      tmin_read;
extern int      tmax_read;
extern int      tins_read;
extern int      tsrc_read;
extern int      t_left;
extern int      t_right;

#define         NMOM            1
#define         NTYPE           4 // dummy constant

extern double  mass[MAXKAP];


extern int     nQueue;
extern int     nAuto;

extern int     max_queue_commands;


#include "index.h"

#define min(a,b) ((a) <= (b) ? (a) : (b))
#define max(a,b) ((a) >= (b) ? (a) : (b))
#define nint(a) ((a) >= 0 ? (int)((a)+0.5) : (int)((a)-0.5))

#include "debug.h"

#include "error.h"

#endif // __GLOBAL_H__
