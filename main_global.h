#ifndef __MAIN_GLOBAL_H__
#define __MAIN_GLOBAL_H__
//==================================================================
// main_global.h :
//      global variables used in functions : main_program,
//                      interrupt_point, save_point and quit_point.
//
// 06/23/2005 [esrevinu] created.
//
//==================================================================
#include <vector>
#include <string>

using namespace std;

#define MAXCOMM 100
#define MAXARGU 100
//=======================
// main_global variables
//=======================

//-----------------------------------------------------------
// For processing the argument list in main_program function
//-----------------------------------------------------------
extern int xargc;
extern char **xargv;

//---------------------------------------------------
// In order to count the number of executed commands
//---------------------------------------------------
extern int nCom;

//------------------------------------------------
// To check whether initialization be done or not
//------------------------------------------------
extern bool init_complete;

//------------------
// name of LockFile
//------------------
extern char72 LockFile;

//-------------------
// name of InputFile
//-------------------
extern char72 InputFile;

//-------------------
// name of OutputFile
//-------------------
extern char72 OutputFile;

//--------------------
// name of LabelFiles
//--------------------
extern char72 LabelFile;

//----------------------
// Full name of program
//----------------------
extern char128 fullprogname;

//---------------------------------
// Directory for saving plot files
//---------------------------------
extern char72 plotDir;

//------------------------------
// Infinite loop to make prompt
//------------------------------
extern bool loop;

//---------------------------------
// FILE pointer for error log file
//---------------------------------
extern FILE *fp_errlog;

//-------------------------
// FILE pointer input file
//-------------------------
extern FILE *fp_input;

//-------------------
// number of buffers
//-------------------
extern int nbuffer, ndbuffer, nlbuffer;

//-------------------
// For fitting range
//------------------
extern int n_1, n_2;


extern int t_1, t_2;
extern int x_1, x_2;
extern double y_1, y_2;

// For the fortran history
//extern char256 hisCom[9];
extern vector<string> hisCom;

//------------------------------------------------------
// If we are in specific buffer, subbuffer and channel,
// then these are average and variance in that location
//------------------------------------------------------
extern double avg[MAXNT], var[MAXNT * MAXNT];


extern double rat[MAXNT], erat[MAXNT];
extern double amp[MAXNT], eamp[MAXNT];
extern double avgx[MAXNT * MAXCFG];
extern double savedpars[MAXPAR], savederrs[MAXPAR], r0, r1, ro0, ro1;

//----------------------
// To check jk working
//----------------------
extern bool jkdone;

extern int nsvd, namoeba, nsing;

//=========================================
// External functions for system interface
//=========================================
extern "C" {
  extern int hold_interrupt(void main_loop(void));
};

extern void preparations(void);
extern void main_loop(void);

//-----------------
// Initializations
//-----------------
extern void init_program(void);

//--------------------------------
// process_argument_list function
//--------------------------------
extern void process_argument_list(int argc, char **argv);

//-------------------
// remove lock files 
//-------------------
extern void remove_lock(void);


#define eprintf(func_name,format,args...) \
                fprintf(stderr, #func_name ": " format "\n", ##args)

#define zero(ptr,sz,type)               \
        do {                            \
          for (int i = 0; i < sz; i++)  \
            *((type *)ptr+i) = 0.;      \
        } while(0)

#define SWAP(a,b) \
    do { a = a + b; b = a - b; a = a - b; } while (0)


#endif                          // __MAIN_GLOBAL_H__
