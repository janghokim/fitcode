//-----------------------------------------------------------------------------
// jk.C  
//             routines related with jk.
//
// 24/08/2006 rvanguard created.
//
//-----------------------------------------------------------------------------
#include <math.h>

#include "global.h"
#include "main_global.h"
#include "jklib.h"

#if ( defined FIT_SWME_m1m2 && defined NAIVE )
//--------------------------------------------------
// jk for SWME of non-degenrate quark mass combination 
//
// kaon masses, eight contractions.
//--------------------------------------------------

//--------------------------------------------------
// Obtain jackKnife replications(mean, variace)
//
// del  : a observation(cfg) which will be deleted
// jkm  : JackKinfe replication of the mean
// jkv  : JackKinfe replication of the variance
//-------------------------------------------------
void jk(int del, double *jkm, double *jkv)
{
  //----------------------------------------
  // data corrected with oscillation factor
  // will be saved in this array
  //----------------------------------------
  double devx[MAXNT*MAXCFG];

  //------------------------------------
  // To calculate with double precision
  //------------------------------------
  double num[MAXNT];

  //---------------------------
  // set the variables to zero 
  //---------------------------
  zero(jkm, MAXNT, double);
  zero(jkv, MAXNT*MAXNT, double);
  zero(devx, MAXNT*MAXCFG, double);
  zero(num, MAXNT, double);

  //-----------------------------
  // To count the number of data
  //-----------------------------
  int nc = 0;

  switch(subbuf) {
  case 0:
    //----------------------------------------------
    // Kaon mass
    //----------------------------------------------
    jk_mass(del, jkm, devx, nc);
    break;
  case 1:
    //----------------------------------------------
    // Eight Contraction and its Vacuum Saturation.
    //----------------------------------------------
    jk_eight(del, jkm, devx, nc);
    break;
  case 2:
    //----------------------------------------------
    // Eight Contraction normalized by AA.
    //----------------------------------------------
    jk_eight_aa(del, jkm, devx, nc);
    break;
  default:
    fputs("jk: under construction\n", stderr);
    break;
  }

  //-------------------------------
  // construct the variance matrix
  //-------------------------------
  for (int t1 = 0; t1 < nt; t1++) {
    for (int t2 = 0; t2 <= t1; t2++) {
      jkv[ind_var(t1,t2)] = 0.0;
      
      for (int cfg = 0; cfg < nc; cfg++) {
        jkv[ind_var(t1,t2)] 
          += (double)devx[ind_devx(t1,cfg)]*(double)devx[ind_devx(t2,cfg)];
      }

      jkv[ind_var(t1,t2)] = jkv[ind_var(t1,t2)] /(double)nc/(double)(nc - 1);
      jkv[ind_var(t2,t1)] = jkv[ind_var(t1,t2)]; 
    }
  }
  return;
}

#elif ( defined FIT_SWME_m1m2 && defined TREE_I2 )
//--------------------------------------------------
// jk for SWME of non-degenrate quark mass combination 
//
// meson analysis.
//--------------------------------------------------

//--------------------------------------------------
// Obtain jackKnife replications(mean, variace)
//
// del  : a observation(cfg) which will be deleted
// jkm  : JackKinfe replication of the mean
// jkv  : JackKinfe replication of the variance
//-------------------------------------------------
void jk(int del, double *jkm, double *jkv)
{
  //----------------------------------------
  // data corrected with oscillation factor
  // will be saved in this array
  //----------------------------------------
  double devx[MAXNT*MAXCFG];

  //---------------------------
  // set the variables to zero 
  //---------------------------
  zero(jkm, MAXNT, double);
  zero(jkv, MAXNT*MAXNT, double);
  zero(devx, MAXNT*MAXCFG, double);

  //-----------------------------
  // To count the number of data
  //-----------------------------
  int nc = 0;

  switch(subbuf) {
  case 0:
    //----------------------------------------------
    // B_K
    //----------------------------------------------
    jk_B_K(del, jkm, devx, nc);
    break;
  case 1:
    //----------------------------------------------
    // B_7 (I=3/2) and O_7 (I=3/2) / AA
    //----------------------------------------------
    jk_B7_I2(del, jkm, devx, nc);
    break;
  case 2:
    //----------------------------------------------
    // B_8 (I=3/2) and O_8 (I=3/2) / AA
    //----------------------------------------------
    jk_B8_I2(del, jkm, devx, nc);
    break;
  default:
    fputs("jk: under construction\n", stderr);
    break;
  }

  //-------------------------------
  // construct the variance matrix
  //-------------------------------
  for (int t1 = 0; t1 < nt; t1++) {
    for (int t2 = 0; t2 <= t1; t2++) {
      jkv[ind_var(t1,t2)] = 0.0;
      
      for (int cfg = 0; cfg < nc; cfg++) {
        jkv[ind_var(t1,t2)] 
          += (double)devx[ind_devx(t1,cfg)]*(double)devx[ind_devx(t2,cfg)];
      }

      jkv[ind_var(t1,t2)] = jkv[ind_var(t1,t2)] /(double)nc/(double)(nc - 1);
      jkv[ind_var(t2,t1)] = jkv[ind_var(t1,t2)]; 
    }
  }
  return;
}

#elif ( (defined FIT_SWME || defined FIT_SWME_m1m2) && defined RENORM_I2 )

void jk(int del, double *jkm, double *jkv)
{
  //----------------------------------------
  // data corrected with oscillation factor
  // will be saved in this array
  //----------------------------------------
  double devx[MAXNT*MAXCFG];

  //---------------------------
  // set the variables to zero 
  //---------------------------
  zero(jkm, MAXNT, double);
  zero(jkv, MAXNT*MAXNT, double);
  zero(devx, MAXNT*MAXCFG, double);

  //-----------------------------
  // To count the number of data
  //-----------------------------
  int nc = 0;

  switch(subbuf) {
  case 0:
    //----------------------------------------------
    // B_K
    //----------------------------------------------
    jk_BK_msbar(del, jkm, devx, nc);
    break;
  case 1:
    //----------------------------------------------
    // B_7 (I=3/2) and O_7 (I=3/2) / AA
    //----------------------------------------------
    jk_B7_I2_msbar(del, jkm, devx, nc);
    break;
  case 2:
    //----------------------------------------------
    // B_8 (I=3/2) and O_8 (I=3/2) / AA
    //----------------------------------------------
    jk_B8_I2_msbar(del, jkm, devx, nc);
    break;
  case 3:
    //----------------------------------------------
    // B_K
    //----------------------------------------------
    jk_BK_msbar_g4(del, jkm, devx, nc);
    break;
  default:
    fputs("jk: under construction\n", stderr);
    break;
  }

  //-------------------------------
  // construct the variance matrix
  //-------------------------------
  for (int t1 = 0; t1 < nt; t1++) {
    for (int t2 = 0; t2 <= t1; t2++) {
      jkv[ind_var(t1,t2)] = 0.0;
      
      for (int cfg = 0; cfg < nc; cfg++) {
        jkv[ind_var(t1,t2)] 
          += (double)devx[ind_devx(t1,cfg)]*(double)devx[ind_devx(t2,cfg)];
      }

      jkv[ind_var(t1,t2)] = jkv[ind_var(t1,t2)] /(double)nc/(double)(nc - 1);
      jkv[ind_var(t2,t1)] = jkv[ind_var(t1,t2)]; 
    }
  }
  return;
}

#elif ( defined PNDME )

void jk(int del, double *jkm, double *jkv)
{
}

#endif

