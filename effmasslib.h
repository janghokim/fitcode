//-------------------------------------------------------------
// effmasslib.h
// 
// Header file for routines for effective masses etc.                           
//
// RCS_id = $Id: effmasslib.h,v 1.2 2015/08/26 09:10:34 jangho Exp $
//
// 02/23/2006 [esrevinu]: created.
//-------------------------------------------------------------

#ifndef __EFFMASSLIB_H__
#define __EFFMASSLIB_H__

//----------------------------
// Calculate effective ratios 
//----------------------------
extern void ratio(const double * avg, const double * var, double * rat,
                  double * erat, int n1, const char *fit, int isamp);

//-------------------------------
// Calculate effective amplitude
//-------------------------------
extern void effamp(const double * avg, const double * var,
                       double * amp, double * eamp, const char *fit);

extern double coshinv(double r, double t, int nt2);
extern double sinhinv(double r, double t, int nt2);
extern double cothinv(double ratt, double y, double yf);
extern double tanhinv(double ratt, double y, double yf);

#endif
