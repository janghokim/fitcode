#ifndef __MINIMIZER_H__
#define __MINIMIZER_H__

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

extern int    ncom;
extern double *pcom;
extern double *xicom;

extern double avgtmpcp[MAXNT];
extern double varinvcp[MAXNT][MAXNT];
extern int    nptcp;

extern void min_newton(double c[MAXPAR],  int ndim, int &iter, int npt,
	    const double avgtmp[MAXNT],  const double varinv[][MAXNT],
	    double &chi2);
	    
extern void min_cg(double c[MAXPAR],  int ndim, int &iter, int npt,
	    const double avgtmp[MAXNT],  const double varinv[][MAXNT],
	    double &chi2); 

extern void dsvdcmp(double a[MAXPAR+1][MAXPAR+1], int m, int n, double w[], 
                    double v[MAXPAR+1][MAXPAR+1]);

extern double dpythag(double a, double b);

extern void linmin_dp(double p[], double xi[],int n, double *fret);

extern void mnbrak_dp(double *ax, double *bx, double *cx, double *fa, 
                  double *fb, double *fc, double (*func)(double));

extern double brent_dp(double ax, double bx, double cx, double (*f)(double), 
                  double tol, double *xmin);
                  
extern double f1dim_dp(double x);

extern double func_chisq(double *p);

#endif
