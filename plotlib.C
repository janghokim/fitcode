//----------------------------------------------------------------------------
// plotlib.C
// 
// 
// 08/08/2005 [rvanguard] : created.
//
// RCS=$Id: plotlib.C,v 1.3 2015/08/31 07:01:09 jangho Exp jangho $
//----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "global.h"
#include "main_global.h"
#include "effmasslib.h"
#include "fitlib.h"
#include "commandlib.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "global_H5.h"
#include "Complex.h"
#include "multi1d.h"
#include "read_data.h"
#include "stat.h"
#include <iostream>
using namespace std ;

void PlotData(string dir, int is_real ){

  //----------------
  // data file write
  //----------------
  char cwd[1024];
  getcwd(cwd, sizeof(cwd)) ;
  string currentdir = cwd ;
  string plotdir = currentdir+"/plot/" + dir ;
  string mkdir = "mkdir -p " + plotdir ;
  system(mkdir.c_str()) ;
  
  string datafile = plotdir+"/"+"data.dat" ;
  
  FILE *fp_data ;
  if((fp_data = fopen(datafile.c_str(), "w"))== NULL){
    cout << "Cannot open " << datafile << endl ;
    return ;
  }

  int nconf = subcha_grp[buffer][subbuf][channel][subcha].getNumObjs() ;
  int nt = num_data[buffer][subbuf][channel][subcha][0] ;

  multi1d<t_cmplx> data(nconf, nt) ;
  for( int iconf = 0 ; iconf < nconf ; iconf++ ) {
    data.subarray(0, iconf);
    read_data(data, buffer, subbuf, channel, subcha, iconf) ;
    data.restore() ;
  }

  //------------------------------
  // average and error calculation
  //------------------------------

  for( int it = 0 ; it < nt ; it++ ) {
    t_cmplx avg, err ;
    data.subarray(1, it) ;
    avg_err(avg, err, data, nconf) ;
    fprintf(fp_data, "%d %.18le %.18le %.18le %.18le\n", 
        it, avg.re, err.re, avg.im, err.im);
//    cout << avg.re << " " << err.re << endl ;
    data.restore() ;
  }
  fclose(fp_data) ;

  //---------
  // eps file
  //---------
  string epsfile = "data.eps" ;
  //---------------------
  // gnuplot script write
  //---------------------
  string gnufile = plotdir+"/"+"data.gnu" ; 
  FILE *fp_gnu ;
  if((fp_gnu = fopen(gnufile.c_str(), "w"))== NULL){
    cout << "Cannot open " << gnufile << endl ;
    return ;
  }
  fprintf(fp_gnu,"set term post eps enhanced font 30\n");
  fprintf(fp_gnu,"set xrange [-1:%d]\n", nt+1);
//  fprintf(fp_gnu,"set yrange [%lf:%lf]\n", min, max);
  fprintf(fp_gnu,"set xlabel \"t\" font \"Times-Roman,30\" \n");
//  fprintf(fp_gnu,"set ylabel \"y\" font \"Times-Roman,30\"  offset 0,0\n");
  fprintf(fp_gnu,"set output \"%s\"\n", epsfile.c_str());
  fprintf(fp_gnu,"plot ");
  if( is_real ){
    fprintf(fp_gnu,"\"data.dat\" using 1:2:3 pt 6 ps 2 lc 1 lt 1 lw 3 w errorbars title \"b%d s%d c%d sc%d\"", buffer+1, subbuf+1, channel+1, subcha+1 );
  }
  else{
    fprintf(fp_gnu,"\"data.dat\" using 1:4:5 pt 6 ps 2 lc 1 lt 1 lw 3 w errorbars title \"b%d s%d c%d sc%d\"", buffer+1, subbuf+1, channel+1, subcha+1 );
  }
  fclose(fp_gnu);
 
  string unixcmd1 = "cd "+currentdir+"/plot/"+dir+"; gnuplot ./data.gnu; evince data.eps > error.log 2>&1 &" ;
  string unixcmd2 = "cd "+currentdir ;

  system(unixcmd1.c_str());
  system(unixcmd2.c_str());
  





  return ;
}



//-------------------------------------------------------
//  plot value of correlator at time slice t vs. configs
//-------------------------------------------------------
void PlotAcor(int t, const double *avgx)
{
  int  icfg, nsamp;
  char filename[64];
  FILE *fp;

  sprintf(filename, "/tmp/xy-acor.%s", pid);

  fprintf(stdout, "Correlator value for time-slice = %d %d %d\n", t+1, t, t-1);
  fp = fopen(filename, "w");
  if (!fp) {
    eprintf(PlotAcor, "can not open file %s", filename);
    return; 
  }                 
  nsamp = ncfg[buffer];
  //------------------------------------------------------------
  //     write header to plot file 
  //------------------------------------------------------------
  fprintf(fp, ";;; file %s\n", filename);
  fprintf(fp, ";;; "); 
  WriteStamp(fp, prog, user, host);
  fprintf(fp, ";;; Header\n");
  fprintf(fp, "#g 0\n#lts 1.2\n#u +0.25\n");
  fprintf(fp, "#lt \"%s\"\n", buflbl[buffer]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u +0.20\n");
  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u +0.15\n");
  fprintf(fp, "#lt \"%s %s p=%s\"\n",
          chalbl[ind_chalbl(0, channel, subbuf, buffer)],
          chalbl[ind_chalbl(1, channel, subbuf, buffer)],
          momlbl[ind_momlbl(channel, subbuf, buffer)]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u +0.10\n");
  fprintf(fp, "#lt \"T =%3d\"\n", t);
  fprintf(fp, "#s\n#g 1\n#lts 1.2\n#u +0.20\n");
//  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, ";;; Plot\n");

  fprintf(fp, "#x \n#y\n#e0\n");

  //--------------------------------------
  // write out the data
  //--------------------------------------
  fprintf(fp, "#c \"\\cr\"\n");
  for (int i = 0; i < nsamp; i++) {
    icfg = cfgtbl[ind_cfgtbl(i, buffer)];
    fprintf(fp, "%5d %-20.8g\n", icfg, avgx[ind_avgx(t-1, i)]);
  }
  fprintf(fp, "#c \"\\oc\"\n");
  for (int i = 0; i < nsamp; i++) {
    icfg = cfgtbl[ind_cfgtbl(i, buffer)];
    fprintf(fp, "%5d %-20.8g\n", icfg, avgx[ind_avgx(t, i)]);
  }
  fprintf(fp, "#c \"\\pl\"\n");
  for (int i = 0; i < nsamp; i++) {
    icfg = cfgtbl[ind_cfgtbl(i, buffer)];
    fprintf(fp, "%5d %-20.8g\n", icfg, avgx[ind_avgx(t+1, i)]);
  }
  fprintf(fp, "#c0\n");
  fprintf(fp, "#m 1\n");
  for (int i = 0; i < nsamp; i++) {
    icfg = cfgtbl[ind_cfgtbl(i, buffer)];
    fprintf(fp, "%5d %-20.8g\n", icfg, avgx[ind_avgx(t, i)]);
  }
  fclose(fp);

  //------------------------------------------------------------
  //     Plot the effective mass along with the fit.
  //------------------------------------------------------------
  if (showPlotFlag) {
    char str[128];
    sprintf(str, "%s %s", plotcommand, filename);
    system(str);
  }
  if (savePlotFlag) {
    char f, c;
    char unixcmd[256];
    char file[256];
    struct stat statbuf;
    if (stat(plotDir, &statbuf) == -1 || !S_ISDIR(statbuf.st_mode)) {
      alert();
      printf("%s: No such directory exists\n", plotDir);
      printf("Do you want to make directory %s? [y/n]", plotDir);
      f = c = getchar();
      while (c != '\n' && c != EOF) c = getchar();
      if (f == 'y' || f == 'Y') {
        sprintf(unixcmd, "%s %s", "mkdir", plotDir);
        printf("%s\n", unixcmd);
        system(unixcmd);
        sprintf(file, "%s/acor_B%02dS%02dC%02d.ax",
                plotDir, buffer+1, subbuf+1, channel+1);
        sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
        printf("%s\n", unixcmd);
        system(unixcmd);
      }
    }
    else {
      sprintf(file, "%s/acor_B%02dS%02dC%02d.ax",
              plotDir, buffer+1, subbuf+1, channel+1);
      sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
      printf("%s\n", unixcmd);
      system(unixcmd);
    }
  }

  return;
}

//------------------------------------------------
//  Plot correlator data and fit to ts = [n1, n2]
//------------------------------------------------
void PlotCor(int n1, int n2, const double *avg, const double *var)
{
  char    *fit;
  int     nt1, nt2;
  double err1[MAXNT], avg1[MAXNT];
  double xx, yy;
  char    filename[64];
  FILE    *fp;

  sprintf(filename, "/tmp/xy-cor1.%s", pid);
  
  nt1 = nt - 1;
  nt2 = (int)(nt/2);
  fit = fitlbl[ind_fitlbl(channel, subbuf, buffer)];

  //--------------------------------------
  // Standard error
  //--------------------------------------
  for (int t = 0; t <= nt1; t++) {
    avg1[t] = avg[t]
      + cxx[ind_cxx(1, channel, subbuf, buffer)] * avg[ind_avg(t, 1)];
    err1[t] = sqrt(var[ind_var(t,t)]);
  }

  fp = fopen(filename, "w");
  if (!fp) {
    eprintf(PlotCor, "can not open file %s", filename);
    return;
  }
  //------------------------------------------------------------
  //     Write header to plot file 
  //------------------------------------------------------------
  fprintf(fp, ";;; file %s\n", filename);
  fprintf(fp, ";;; ");
  WriteStamp(fp, prog, user, host);
  fprintf(fp, ";;; Header\n");
  fprintf(fp, "#y l\n#g 0\n#lts 1.2\n");
  fprintf(fp, "#lt \"%s\"\n", buflbl[buffer]);
  fprintf(fp, "#s\n#y l\n#g 0\n#lts 1.2\n#u -0.05\n");
  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, "#s\n#y l\n#g 0\n#lts 1.2\n#u -0.10\n");
  fprintf(fp, "#lt \"%s %s p=%s\"\n#s\n#lts 0\n#g 1\n",
          chalbl[ind_chalbl(0, channel, subbuf, buffer)],
          chalbl[ind_chalbl(1, channel, subbuf, buffer)],
          momlbl[ind_momlbl(channel, subbuf, buffer)]);
  fprintf(fp, ";;; Plot\n");
  fprintf(fp, "#x 0 %d\n", nt1);
  if (strncmp(fit, "const", 5) != 0) fprintf(fp, "#y l\n");
  else fprintf(fp, "#y \n");
  fprintf(fp, "#e\n");
  //------------------------------------------------------------
  //     Write data 
  //------------------------------------------------------------
  for (int t = 0; t <= 1; t++) {
    if (fabs(avg1[t]) > err1[t]) {
      if (avg1[t] > 0) fprintf(fp, "; %d %20.8e %20.8e \"\\cr\"\n", 
			       t, avg1[t], err1[t]);
      else if (avg1[t] < 0) fprintf(fp, "; %d %20.8e %20.8e \"\\pl\"\n",
				    t, fabs(avg1[t]), err1[t]);
    }
    else if (avg1[t] != 0.0) fprintf(fp, "; %d %20.8e %20.8e \"\\bu\"\n",
				     t, fabs(avg1[t]), 0.0);
  }
  for (int t = 2; t <= nt-3; t++) {
    if (fabs(avg1[t]) > err1[t]) {
      if (avg1[t] > 0) fprintf(fp, "%d %20.8e %20.8e \"\\cr\"\n", 
			       t, avg1[t], err1[t]);
      else if (avg1[t] < 0) fprintf(fp, "%d %20.8e %20.8e \"\\pl\"\n",
				    t, fabs(avg1[t]), err1[t]);
    }
    else if (avg1[t] != 0.0) fprintf(fp, "%d %20.8e %20.8e \"\\bu\"\n",
				     t, fabs(avg1[t]), 0.0);
  }
  for (int t = nt-3; t <= nt1; t++) {
    if (fabs(avg1[t]) > err1[t]) {
      if (avg1[t] > 0) fprintf(fp, "; %d %20.8e %20.8e \"\\cr\"\n", 
			       t, avg1[t], err1[t]);
      else if (avg1[t] < 0) fprintf(fp, "; %d %20.8e %20.8e \"\\pl\"\n",
				    t, fabs(avg1[t]), err1[t]);
    }
    else if (avg1[t] != 0.0) fprintf(fp, "; %d %20.8e %20.8e \"\\bu\"\n",
				     t, fabs(avg1[t]), 0.0);
  }
  //------------------------------------------------------------
  //     Write the fie points 
  //------------------------------------------------------------
  fprintf(fp, "#e0 \n#c0\n#m 0\n");
  for (int t = 0; t <= nt1; t++) {
    xx = gfit(fit, t,   &cxx[ind_cxx(0,channel, subbuf, buffer)]);
    yy = gfit(fit, t+1, &cxx[ind_cxx(0,channel, subbuf, buffer)]);
    if (xx > 0 && yy > 0) {
      if (t < n1) {
	fprintf(fp, "#m 2\n%3d     %20.8e\n", t, xx);
	fprintf(fp, "%3d     %20.8e\n", t+1, yy);
      }
      else if (t >= n1 && t < n2) {
	fprintf(fp, "#m 1\n%3d     %20.8e\n", t, xx);
	fprintf(fp, "%3d     %20.8e\n", t+1, yy);
      }
      else if (t >= n2 && t < nt2) {
	fprintf(fp, "#m 2\n%3d     %20.8e\n", t, xx);
	fprintf(fp, "%3d     %20.8e\n", t+1, yy);
      }
    }
    if (xx < 0 && yy < 0) {
      if (t > n2 && t < n1) {
	fprintf(fp, "#m 2\n%3d     %20.8e\n", t, fabs(xx));
	fprintf(fp, "%3d     %20.8e\n", t+1, fabs(yy));
      }
      else if (t >= n1 && t < n2) {
	fprintf(fp, "#m 1\n%3d     %20.8e\n", t, fabs(xx));
	fprintf(fp, "%3d     %20.8e\n", t+1, fabs(yy));
      }
      else if (t >= n2 && t < nt1) {
	fprintf(fp, "#m 2\n%3d     %20.8e\n", t, fabs(xx));
	fprintf(fp, "%3d     %20.8e\n", t+1, fabs(yy));
      }
    }
  }
  fclose(fp);
  
  //------------------------------------------------------------
  //     Plot the effective mass along with the fit.
  //------------------------------------------------------------
  if (showPlotFlag) {
    char str[128];
    sprintf(str, "%s %s", plotcommand, filename);
    system(str);
  }
  if (savePlotFlag) {
    char f, c;
    char unixcmd[256];
    char file[256];
    struct stat statbuf;
    if (stat(plotDir, &statbuf) == -1 || !S_ISDIR(statbuf.st_mode)) {
      alert();
      printf("%s: No such directory exists\n", plotDir);
      printf("Do you want to make directory %s? [y/n]", plotDir);
      f = c = getchar();
      while (c != '\n' && c != EOF) c = getchar();
      if (f == 'y' || f == 'Y') {
        sprintf(unixcmd, "%s %s", "mkdir", plotDir);
        printf("%s\n", unixcmd);
        system(unixcmd);
        sprintf(file, "%s/cor_B%02dS%02dC%02d.ax",
                plotDir, buffer+1, subbuf+1, channel+1);
        sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
        printf("%s\n", unixcmd);
        system(unixcmd);
      }
    }
    else {
      sprintf(file, "%s/cor_B%02dS%02dC%02d.ax",
              plotDir, buffer+1, subbuf+1, channel+1);
      sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
      printf("%s\n", unixcmd);
      system(unixcmd);
    }
  }
}

//-----------------------------------------
//  Calculate and plot effective amplitude
//-----------------------------------------
void PlotAmp(int n1, int n2, int t1, int t2, int x1, int x2,
             double y1, double y2, const double * amp,
             const double * eamp)
{
  double c[MAXPAR] = { 0 };
  int nt1, nt2;
  char filename[64];
  FILE *fp;

  c[0] = 1;
  sprintf(filename, "/tmp/xy-amp.%s", pid);

  nt1 = nt - 1;
  nt2 = nt / 2;

  fp = fopen(filename, "w");
  if (!fp) {
    eprintf(PlotAmp, "can not open file %s", filename);
    return;
  }

  //------------------------------------------------------------
  //     write header to plot file 
  //------------------------------------------------------------
  fprintf(fp, ";;; file %s\n", filename);
  fprintf(fp, ";;; ");
  WriteStamp(fp, prog, user, host);
  fprintf(fp, ";;; Header\n");
  fprintf(fp, "#g 0\n#lts 1.2\n");
  fprintf(fp, "#lt \"%s\"\n", buflbl[buffer]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u -0.05\n");
  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u -0.10\n");
  fprintf(fp, "#lt \"%s %s p=%s\"\n#s\n#lts 0\n#g 1\n",
          chalbl[ind_chalbl(0, channel, subbuf, buffer)],
          chalbl[ind_chalbl(1, channel, subbuf, buffer)],
          momlbl[ind_momlbl(channel, subbuf, buffer)]);
  fprintf(fp, ";;; Plot\n");

  fprintf(fp, "#x %d %d\n", x1, x2);
  if (fabs(y1 - y2) < 1.0e-10)
    fprintf(fp, "#y\n");
  else
    fprintf(fp, "#y %20.8e %20.8e\n", y1, y2);

  fprintf(fp, "#e\n");

  //fit = fitlbl[ind_fitlbl(channel,subbuf,buffer)];

  //------------------------------------------------------------
  //       Plot effective amplitude for cosh (sinh) fit.
  //------------------------------------------------------------
  fprintf(fp, "#c \"\\cr\" \n");

  for (int t = 0; t < nt; t++) {
    if (t < t1 || t > t2)
      fprintf(fp, ";%2d    %20.8e%20.8e\n", t, amp[t], eamp[t]);
    else
      fprintf(fp, " %2d     %20.8e%20.8e\n", t, amp[t], eamp[t]);
  }

  //------------------------------------------------------------
  //     Draw the JK answer.
  //------------------------------------------------------------
  fprintf(fp, "#e0\n");
  fprintf(fp, "#c0\n");
  fprintf(fp, "#m 1\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d    %20.8e\n", t,
            cxx[ind_cxx(0, channel, subbuf, buffer)]);
  }
  fprintf(fp, "#m 2\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d     %20.8e\n", t,
            cxx[ind_cxx(0, channel, subbuf, buffer)]
            + cerrxx[ind_cerrxx(0, channel, subbuf, buffer)]);
  }
  fprintf(fp, "#m 2\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d     %20.8e\n", t,
            cxx[ind_cxx(0, channel, subbuf, buffer)]
            - cerrxx[ind_cerrxx(0, channel, subbuf, buffer)]);
  }
  fclose(fp);

  //------------------------------------------------------------
  //     Plot the effective mass along with the fit.
  //------------------------------------------------------------
  if (showPlotFlag) {
    char str[128];
    sprintf(str, "%s %s", plotcommand, filename);
    system(str);
  }
  if (savePlotFlag) {
    char f, c;
    char unixcmd[256];
    char file[256];
    struct stat statbuf;
    if (stat(plotDir, &statbuf) == -1 || !S_ISDIR(statbuf.st_mode)) {
      alert();
      printf("%s: No such directory exists\n", plotDir);
      printf("Do you want to make directory %s? [y/n]", plotDir);
      f = c = getchar();
      while (c != '\n' && c != EOF) c = getchar();
      if (f == 'y' || f == 'Y') {
        sprintf(unixcmd, "%s %s", "mkdir", plotDir);
        printf("%s\n", unixcmd);
        system(unixcmd);
        sprintf(file, "%s/amp_B%02dS%02dC%02d.ax",
                plotDir, buffer+1, subbuf+1, channel+1);
        sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
        printf("%s\n", unixcmd);
        system(unixcmd);
      }
    }
    else {
      sprintf(file, "%s/amp_B%02dS%02dC%02d.ax",
              plotDir, buffer+1, subbuf+1, channel+1);
      sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
      printf("%s\n", unixcmd);
      system(unixcmd);
    }
  }
}


//----------------------------------------------------
//  Calculate and plot effective mass (or equivalent)
//----------------------------------------------------
void PlotRatio(int n1, int n2, int t1, int t2, int x1, int x2,
               double y1, double y2, const double * rat,
               const double * erat)
{
  char *fit;
  int nt1, nt2, ipar;
  char filename[64];
  FILE *fp;

  sprintf(filename, "/tmp/xy-ratio.%s", pid);

  nt1 = nt - 1;
  nt2 = nt / 2;

  fp = fopen(filename, "w");
  if (!fp) {
    eprintf(PlotRatio, "can not open file %s", filename);
    return;
  }

  //------------------------------------------------------------
  //     write header to plot file 
  //------------------------------------------------------------
  fprintf(fp, ";;; file %s\n", filename);
  fprintf(fp, ";;; ");
  WriteStamp(fp, prog, user, host);
  fprintf(fp, ";;; Header\n");
  fprintf(fp, "#g 0\n#lts 1.2\n#u +0.25\n");
  fprintf(fp, "#lt \"%s\"\n", buflbl[buffer]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u +0.20\n");
  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u +0.15\n");
  fprintf(fp, "#lt \"p=%s\"\n#s\n#lts 0\n#g 1\n",
          momlbl[ind_momlbl(channel, subbuf, buffer)]);
  fprintf(fp, "#ly \"%s\"\n",
          chalbl[ind_chalbl(0, channel, subbuf, buffer)]);
  fprintf(fp, "#lx \"%s\"\n",
          chalbl[ind_chalbl(1, channel, subbuf, buffer)]);
  fprintf(fp, ";;; Plot\n");

  fprintf(fp, "#x %d %d\n", x1, x2);
  if (fabs(y1 - y2) < 1.0e-10)
    fprintf(fp, "#y\n");
  else
    fprintf(fp, "#y %20.8e %20.8e\n", y1, y2);

  fprintf(fp, "#e\n");

  fit = fitlbl[ind_fitlbl(channel, subbuf, buffer)];

  if (strncmp(fit, "cosh", 4) == 0 || strcmp(fit, "sinh") == 0
      || strcmp(fit, "newt+") == 0 || strcmp(fit, "newt-") == 0
      || strcmp(fit, "cosh.v2") == 0 || strcmp(fit, "sinh.v2") == 0) {
    //------------------------------------------------------------
    //      Plot effective mass for cosh (sinh) fit.
    //      Assumes (anti)symmetrization in time.
    //------------------------------------------------------------
    fprintf(fp, "#c \"\\cr\" \n");

    for (int t = 0; t <= nt2; t++) {
      if (t < t1 || t > t2)
        fprintf(fp, ";%2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
      else
        fprintf(fp, " %2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
    }
  } 

  else {
    if (n1 < nt2) {
      fprintf(fp, "#c \"\\cr\" \n");

      for (int t = 0; t <= nt2; t++) {
        if (t < t1 || t > t2)
          fprintf(fp, ";%2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
        else
          fprintf(fp, " %2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
      }
    } 
    else {
      fprintf(fp, "#c \"\\oc\" \n");

      for (int t = 0; t <= nt2; t++) {
        if (t < t1 || t > t2)
          fprintf(fp, ";%2d     %20.8e%20.8e\n", nt - t, rat[nt - t],
                  erat[nt - t]);
        else
          fprintf(fp, " %2d     %20.8e%20.8e\n", nt - t, rat[nt - t],
                  erat[nt - t]);
      }
    }
  }

  //---------------------------------------------------------------
  // Choose parameter to display (1 for 'const' fit, 2 otherwise).
  //---------------------------------------------------------------
  if (strncmp(fit, "const", 5) == 0 || strncmp(fit, "minca", 5) == 0) 
    ipar = 0;
  else if (strcmp(fit, "mes-") == 0 || strcmp(fit, "newt-") == 0) 
    ipar = 3;
  else 
    ipar = 1;

  //------------------------------------------------------------
  //     Draw the JK answer.
  //------------------------------------------------------------
  fprintf(fp, "#e0\n");
  fprintf(fp, "#c0\n");
  fprintf(fp, "#m 1\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d     %20.8e\n", t,
            cxx[ind_cxx(ipar, channel, subbuf, buffer)]);
  }
  fprintf(fp, "#m 2\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d     %20.8e\n", t,
            cxx[ind_cxx(ipar, channel, subbuf, buffer)]
            + cerrxx[ind_cerrxx(ipar, channel, subbuf, buffer)]);
  }
  fprintf(fp, "#m 2\n");
  for (int t = n1; t <= n2; t++) {
    fprintf(fp, "%3d     %20.8e\n", t,
            cxx[ind_cxx(ipar, channel, subbuf, buffer)]
            - cerrxx[ind_cerrxx(ipar, channel, subbuf, buffer)]);
  }
  fclose(fp);

  //------------------------------------------------------------
  //     Plot the effective mass along with the fit.
  //------------------------------------------------------------
  if (showPlotFlag) {
    char str[128];
    sprintf(str, "%s %s", plotcommand, filename);
    system(str);
  }
  if (savePlotFlag) {
    char f, c;
    char unixcmd[256];
    char file[256];
    struct stat statbuf;
    if (stat(plotDir, &statbuf) == -1 || !S_ISDIR(statbuf.st_mode)) {
      alert();
      printf("%s: No such directory exists\n", plotDir);
      printf("Do you want to make directory %s? [y/n]", plotDir);
      f = c = getchar();
      while (c != '\n' && c != EOF) c = getchar();
      if (f == 'y' || f == 'Y') {
        sprintf(unixcmd, "%s %s", "mkdir", plotDir);
        printf("%s\n", unixcmd);
        system(unixcmd);
        sprintf(file, "%s/r_B%02dS%02dC%02d.ax",
                plotDir, buffer+1, subbuf+1, channel+1);
        sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
        printf("%s\n", unixcmd);
        system(unixcmd);
      }
    }
    else {
      sprintf(file, "%s/r_B%02dS%02dC%02d.ax",
              plotDir, buffer+1, subbuf+1, channel+1);
      sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
      printf("%s\n", unixcmd);
      system(unixcmd);
    }
  }
}

//---------------------------------------------------------------------------
//    Calculate and plot effective mass (or equivalent).
//---------------------------------------------------------------------------
void PlotRRatio(int n1, int n2, int t1, int t2, int x1, int x2,
                double y1, double y2, const double * rat,
                const double * erat)
{
  char *fit;
  double y, yo, yn, r;
  double float_t, float_tr, rr;
  int nt1, nt2, nt4;
  char filename[64];
  FILE *fp;

  sprintf(filename, "/tmp/xy-ratio.%s", pid);

  nt1 = nt - 1;
  nt2 = nt / 2;
  nt4 = nt / 4;

  fp = fopen(filename, "w");
  if (!fp) {
    eprintf(PlotRRatio, "can not open file %s", filename);
    return;
  }

  //------------------------------------------------------------
  //     write header to plot file 
  //------------------------------------------------------------
  fprintf(fp, ";;; file %s\n", filename);
  fprintf(fp, ";;; ");
  WriteStamp(fp, prog, user, host);
  fprintf(fp, ";;; Header\n");

  fprintf(fp, "#g 0\n#lts 1.2\n");
  fprintf(fp, "#lt \"%s\"\n", buflbl[buffer]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u -0.05\n");
  fprintf(fp, "#lt \"%s\"\n", sublbl[ind_sublbl(subbuf, buffer)]);
  fprintf(fp, "#s\n#g 0\n#lts 1.2\n#u -0.10\n");
  fprintf(fp, "#lt \"%s %s p=%s\"\n#s\n#lts 0\n#g 1\n",
          chalbl[ind_chalbl(0, channel, subbuf, buffer)],
          chalbl[ind_chalbl(1, channel, subbuf, buffer)],
          momlbl[ind_momlbl(channel, subbuf, buffer)]);
  fprintf(fp, ";;; Plot\n");

  fprintf(fp, "#x %d %d\n", x1, x2);
  if (fabs(y1 - y2) < 1.0e-10)
    fprintf(fp, "#y\n");
  else
    fprintf(fp, "#y, %20.8e %20.8e\n", y1, y2);
  fprintf(fp, "#e\n");

  fit = fitlbl[ind_fitlbl(channel, subbuf, buffer)];

  if (strncmp(fit, "cosh", 4) == 0 || strcmp(fit, "sinh") == 0
      || strcmp(fit, "mes+") == 0 || strcmp(fit, "mes-") == 0
      || strcmp(fit, "newt+") == 0 || strcmp(fit, "newt-") == 0
      || strcmp(fit, "cosh.v2") == 0 || strcmp(fit, "sinh.v2") == 0) {
    //------------------------------------------------------------
    //      Plot effective mass for cosh (sinh) fit.
    //      Assumes (anti)symmetrization in time.
    //------------------------------------------------------------
    fprintf(fp, "#c \"\\cr\" \n");

    for (int t = 0; t <= nt2; t++) {
      if (t < t1 || t > t2)
        fprintf(fp, ";%2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
      else
        fprintf(fp, " %2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
    }
  } else {
    if (n1 < nt2) {
      fprintf(fp, "#c \"\\cr\" \n");

      for (int t = 0; t <= nt2; t++) {
        if (t < t1 || t > t2)
          fprintf(fp, ";%2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
        else
          fprintf(fp, " %2d     %20.8e%20.8e\n", t, rat[t], erat[t]);
      }
    } else {
      fprintf(fp, "#c \"\\oc\" \n");

      for (int t = 0; t <= nt2; t++) {
        if (t < t1 || t > t2)
          fprintf(fp, ";%2d     %20.8e%20.8e\n", nt - t, rat[nt - t],
                  erat[nt - t]);
        else
          fprintf(fp, " %2d     %20.8e%20.8e\n", nt - t, rat[nt - t],
                  erat[nt - t]);
      }
    }
  }

  //------------------------------------------------------------
  //     Draw the JK answer.
  //------------------------------------------------------------
  fprintf(fp, "#e0\n");
  fprintf(fp, "#c0\n");
  fprintf(fp, "#m 1\n");

  yo = gfit(fit, n1 - 1, &cxx[ind_cxx(0, channel, subbuf, buffer)]);
  y = gfit(fit, n1, &cxx[ind_cxx(0, channel, subbuf, buffer)]);
  for (int t = n1; t <= n2; t++) {
    yn = gfit(fit, t + 1, &cxx[ind_cxx(0, channel, subbuf, buffer)]);
    rr = yo / y;
    r = rr;
    float_t = t;
    float_tr = (nt2 - float_t) / (nt2 - float_t + 1);
    if (strcmp(fit, "cosh") == 0 || strcmp(fit, "C**2/C(cc)") == 0
        || strcmp(fit, "C**2/C(css)") == 0) {
      if (fabs(y) < fabs(yo)) {
        fprintf(fp, ";%2d     %20.8e\n", t,
                (double) coshinv(rr, float_t, nt / 2));
      }
    } else if (strcmp(fit, "sinh") == 0 || strcmp(fit, "C**2/C(ss)") == 0
               || strcmp(fit, "C**2/C(csc)") == 0) {
      if (fabs(y) < fabs(yo)) {
        fprintf(fp, ";%2d     %20.8e\n", t,
                (double) sinhinv(rr, float_t, nt / 2));
      }
    } else if (strcmp(fit, "mes+") == 0 || strcmp(fit, "newt+") == 0
               || strcmp(fit, "cosh.v2") == 0 || strcmp(fit, "sinh.v2") == 0) {
      fprintf(fp, "%2d     %20.8e\n", t,
              (double) cxx[ind_cxx(1, channel, subbuf, buffer)]);
    } else if (strcmp(fit, "mes-") == 0 || strcmp(fit, "newt-") == 0) {
      fprintf(fp, "%2d     %20.8e\n", t,
              (double) cxx[ind_cxx(3, channel, subbuf, buffer)]);
    } else if (strcmp(fit, "exp") == 0 || strcmp(fit, "baryon") == 0
               || strcmp(fit, "cosh+const/cosh^2") == 0) {
      if (r > 0)
        fprintf(fp, ";%2d     %20.8e\n", t, log(r));
    } else if (strcmp(fit, "gexp") == 0) {
      if (fabs(yo) < fabs(y))
        fprintf(fp, ";%2d     %20.8e\n", t, -log(r));
    } else if (strcmp(fit, "linear") == 0) {
      fprintf(fp, ";%2d     %20.8e\n", t, yn - y);
    } else if (strcmp(fit, "const(c/s)") == 0) {
      if (yo != 0 && y != 0 && fabs(y) > fabs(yo) && t > nt4)
        fprintf(fp, ";%2d     %20.8e\n", t,
                (double) cothinv(float_tr, yo, y));
      else
        fprintf(fp, ";%2d     %20.8e\n", t, y);
    } else if (strcmp(fit, "const(s/c)") == 0) {
      if (yo != 0 && y != 0 && fabs(y) < fabs(yo) && t > nt4)
        fprintf(fp, ";%2d     %20.8e\n", t,
                (double) tanhinv(float_tr, yo, y));
      else
        fprintf(fp, ";%2d     %20.8e\n", t, y);
    } else if (strncmp(fit, "const", 5) == 0) {
      fprintf(fp, ";%2d     %20.8e\n", t, y);
    } else if (strncmp(fit, "minca", 5) == 0) {
      fprintf(fp, ";%2d     %20.8e\n", t, y);
    } else if (strncmp(fit, "C**2/C", 6) == 0) {
      if (fabs(y) < fabs(yo))
        fprintf(fp, ";%2d     %20.8e\n", t, log(r));
    } else if (strcmp(fit, "cosh+const") == 0) {
      r = (yo - y) / (y - yn);
      if (fabs(y) < fabs(yo))
        fprintf(fp, ";%2d     %20.8e\n", t,
                (double) sinhinv(rr, float_t, nt / 2));
    } else {                    // God knows what fit type this is!
      if (fabs(y) < fabs(yo))
        fprintf(fp, ";%2d     %20.8e\n", t, log(r));
    }
    yo = y;
    y = yn;
  }
  fclose(fp);

  //------------------------------------------------------------
  //     Plot the effective mass along with the fit.
  //------------------------------------------------------------
  if (showPlotFlag) {
    char str[128];
    sprintf(str, "%s %s", plotcommand, filename);
    system(str);
  }
  if (savePlotFlag) {
    char f, c;
    char unixcmd[256];
    char file[256];
    struct stat statbuf;
    if (stat(plotDir, &statbuf) == -1 || !S_ISDIR(statbuf.st_mode)) {
      alert();
      printf("%s: No such directory exists\n", plotDir);
      printf("Do you want to make directory %s? [y/n]", plotDir);
      f = c = getchar();
      while (c != '\n' && c != EOF) c = getchar();
      if (f == 'y' || f == 'Y') {
        sprintf(unixcmd, "%s %s", "mkdir", plotDir);
        printf("%s\n", unixcmd);
        system(unixcmd);
        sprintf(file, "%s/rr_B%02dS%02dC%02d.ax",
                plotDir, buffer+1, subbuf+1, channel+1);
        sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
        printf("%s\n", unixcmd);
        system(unixcmd);
      }
    }
    else {
      sprintf(file, "%s/rr_B%02dS%02dC%02d.ax",
              plotDir, buffer+1, subbuf+1, channel+1);
      sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
      printf("%s\n", unixcmd);
      system(unixcmd);
    }
  }
}


//--------------------------------------------------------------------------
// move plot to a safe file.
//--------------------------------------------------------------------------
void move(char *PlotType, char *file)
{
  char unixcmd[255];

  if (PlotType[0] == 'a') {
    sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-acor.", pid, file);
    system(unixcmd);
  } else if (PlotType[0] == 'c') {
    sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-cor1.", pid, file);
    system(unixcmd);
  } else if (PlotType[0] == 'r') {
    sprintf(unixcmd, "%s %s%s %s", "cp", "/tmp/xy-ratio.", pid, file);
    printf("%s\n", unixcmd);
    system(unixcmd);
  } 
  else 
    ErrMsg("move: Unknown plot type\n", stderr);
}

//-------------------
// Adjust plot range
//-------------------
int AdjustPlotRange(char256 *argu)
{
  int i = 1;
  char *arg_i0;
  char *p1, *p2;

  while ((arg_i0 = argu[i]) && i <= 9) {

    if (arg_i0[0] == '\0') return 1;

    //----------------
    // Adjust x range
    //----------------
    else if (strcmp(arg_i0, "-x") == 0) {
      x_1 = strtol(argu[++i], &p1, 10);
      x_2 = strtol(argu[++i], &p2, 10);

      if (*p1 != '\0' || *p2 != '\0') return 0;
      else if (x_1 >= x_2) {
        ErrMsg("Plot: x-axis plot range is improper\n");
        return 0;
      }
    } 

    //----------------
    // Adjust y range
    //----------------
    else if (strcmp(arg_i0, "-y") == 0) {
      y_1 = strtof(argu[++i], &p1);
      y_2 = strtof(argu[++i], &p2);

      if (*p1 != '\0' && *p2 != '\0') return 0;
      else if (y_1 >= y_2) {
        ErrMsg("Plot: y-axis plot range is improper\n");
        return 0;
      }
    } 

    //------------------------
    // restore default values
    //------------------------
    else if (strcmp(arg_i0, "-") == 0) {
      t_1 = 1; 
      t_2 = nt / 2 - 1;
      x_1 = 0; 
      x_2 = nt / 2;
      y_1 = 0.0;
      y_2 = 0.0;
      return 1;
    }

    //-------------------------
    // restrict the plot range
    //-------------------------
    else {
      t_1 = strtol(argu[i], &p1, 10);
      t_2 = strtol(argu[++i], &p2, 10);

      if (*p1 != '\0' || *p2 != '\0') return 0;
      else if (t_1 >= t_2) {
        ErrMsg("Plot: t range is improper\n");
        return 0;
      }
    }
    i++;
  }
  return 1;
}
